SOURCE_DIR = src
INCLUDE_DIR = include
TEST_DIR = test
BUILD_DIR = build
REPORT_DIR = $(BUILD_DIR)/report

_TARGETS = Mfx_Basic Mfx_Compound

TARGETS = $(addprefix $(BUILD_DIR)/,$(_TARGETS))
COMMON_HEADER_FILES = $(wildcard $(INCLUDE_DIR)/*)

CC = gcc
CFLAGS = -Wall -Wextra -I$(INCLUDE_DIR) --coverage
LDFLAGS = --coverage

EMPTY =
SPACE = $(EMPTY) $(EMPTY)

.PHONY: all
all: $(TARGETS)
	cppcheck --addon=cert --cppcheck-build-dir=$(BUILD_DIR) $(patsubst $(BUILD_DIR)/%,$(SOURCE_DIR)/%.c,$^)
	@rm -f $(BUILD_DIR)/*.gcda
	$(patsubst %,%;,$^)
	gcov $(patsubst %,%.c,$^)
	@mv *.gcov $(BUILD_DIR)

.PHONY: report
report: all | $(REPORT_DIR)
	gcovr -r . --print-summary --filter '($(SOURCE_DIR)|$(TEST_DIR))/(Test_)*($(subst $(SPACE),|,$(_TARGETS)))\.c' --html-details -o $(REPORT_DIR)/coverage.html

.PHONY: doc
doc:
	doxygen Doxyfile

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) doc

$(TARGETS): $(BUILD_DIR)/%: $(BUILD_DIR)/%.o $(BUILD_DIR)/Test_%.o
	$(CC) -o $@ $^ $(LDFLAGS)

$(BUILD_DIR)/Test_%.o: $(TEST_DIR)/Test_%.c $(COMMON_HEADER_FILES) | $(BUILD_DIR)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.c $(COMMON_HEADER_FILES) | $(BUILD_DIR)
	$(CC) -c -o $@ $< $(CFLAGS)

$(BUILD_DIR) $(REPORT_DIR):
	@mkdir -p $@
