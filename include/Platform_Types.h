#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

#include <stdint.h>

typedef uint8_t boolean;

typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

typedef int8_t sint8;
typedef int16_t sint16;
typedef int32_t sint32;
typedef int64_t sint64;

typedef uint_least8_t uint8_least;
typedef uint_least16_t uint16_least;
typedef uint_least32_t uint32_least;

typedef int_least8_t sint8_least;
typedef int_least16_t sint16_least;
typedef int_least32_t sint32_least;

typedef float float32;
typedef double float64;

typedef void *VoidPtr;
typedef const void *ConstVoidPtr;

typedef enum { CPU_TYPE_8, CPU_TYPE_16, CPU_TYPE_32, CPU_TYPE_64 } CPU_TYPE;

typedef enum { MSB_FIRST, LSB_FIRST } CPU_BIT_ORDER;

typedef enum { HIGH_BYTE_FIRST, LOW_BYTE_FIRTS } CPU_BYTE_ORDER;

#ifndef TRUE
#    define TRUE 1
#endif

#ifndef FALSE
#    define FALSE 0
#endif

#endif /* PLATFORM_TYPES_H */
