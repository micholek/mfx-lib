#ifndef MFX_H
#define MFX_H

#include "Std_Types.h"

/**
 * @defgroup generalMacros General macros
 *
 * General macros.
 */
/**@{*/
/**
 * @brief Major version number of the vendor specific implementation of the
 * module.
 **/
#define MFX_SW_MAJOR_VERSION (1)
/**
 * @brief Minor version number of the vendor specific implementation of the
 * module.
 **/
#define MFX_SW_MINOR_VERSION (0)
/**
 * @brief Patch version number of the vendor specific implementation of the
 * module.
 **/
#define MFX_SW_PATCH_VERSION (0)
/**
 * @brief Major release version number of the AUTOSAR specification which the
 * implementation is based on.
 **/
#define MFX_AR_RELEASE_MAJOR_VERSION (4)
/**
 * @brief Minor release version number of the AUTOSAR specification which the
 * implementation is based on.
 **/
#define MFX_AR_RELEASE_MINOR_VERSION (7)
/**
 * @brief Patch release version number of the AUTOSAR specification which the
 * implementation is based on.
 **/
#define MFX_AR_RELEASE_REVISION_VERSION (0)

/** @brief Module ID. */
#define MFX_MODULE_ID (211)

/** @brief Vendor ID. */
#define MFX_VENDOR_ID (1)

/**
 * @brief This macro provides the library version information.
 *
 * @param versioninfo Pointer to where to store the version information of this
 * module. Format according to [BSW00321].
 */
#define Mfx_GetVersionInfo(versioninfo)                \
    do {                                               \
        *(versioninfo) = (const Std_VersionInfoType) { \
            .moduleID = MFX_MODULE_ID,                 \
            .vendorID = MFX_VENDOR_ID,                 \
            .sw_major_version = MFX_SW_MAJOR_VERSION,  \
            .sw_minor_version = MFX_SW_MINOR_VERSION,  \
            .sw_patch_version = MFX_SW_PATCH_VERSION,  \
        };                                             \
    } while (0)

/** @brief Minimum value of a signed 8-bit integer. */
#define MFX_SINT8_MIN (-128)
/** @brief Maximum value of a signed 8-bit integer. */
#define MFX_SINT8_MAX (127)
/** @brief Minimum value of an unsigned 8-bit integer. */
#define MFX_UINT8_MIN (0)
/** @brief Maximum value of an unsigned 8-bit integer. */
#define MFX_UINT8_MAX (255)
/** @brief Minimum value of a signed 16-bit integer. */
#define MFX_SINT16_MIN (-32768)
/** @brief Maximum value of a signed 16-bit integer. */
#define MFX_SINT16_MAX (32767)
/** @brief Minimum value of an unsigned 16-bit integer. */
#define MFX_UINT16_MIN (0)
/** @brief Maximum value of an unsigned 16-bit integer. */
#define MFX_UINT16_MAX (65535)
/** @brief Minimum value of a signed 32-bit integer. */
#define MFX_SINT32_MIN (-2147483648L)
/** @brief Maximum value of a signed 32-bit integer. */
#define MFX_SINT32_MAX (2147483647L)
/** @brief Minimum value of an unsigned 32-bit integer. */
#define MFX_UINT32_MIN (0UL)
/** @brief Maximum value of an unsigned 32-bit integer. */
#define MFX_UINT32_MAX (4294967295UL)
/**@}*/

/**
 * @defgroup additions Additions
 *
 * These routines make an addition between the two arguments.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x005 */
sint8 Mfx_Add_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x014 */
sint32 Mfx_Add_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup substractions Substractions
 *
 * These routines make a substraction between the two arguments.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x029 */
sint8 Mfx_Sub_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x045 */
sint32 Mfx_Sub_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup absoluteValue Absolute value
 *
 * These routines compute the absolute value of a signed value.
 *
 * @param x_value First argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x056 */
sint8 Mfx_Abs_s8_s8(sint8 x_value);
/** @brief ID: 0x05C */
sint32 Mfx_Abs_s32_s32(sint32 x_value);
/**@}*/

/**
 * @defgroup absoluteValueOfADifference Absolute value of a difference
 *
 * These routines compute the absolute value of a difference between 2 values.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x082 */
sint8 Mfx_AbsDiff_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x069 */
sint32 Mfx_AbsDiff_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup multiplications Multiplications
 *
 * These routines make a multiplication between the two arguments.
 *
 * @param x_value First argument.
 * @param x_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x085 */
sint8 Mfx_Mul_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x094 */
sint32 Mfx_Mul_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup divisionsRoundedTowards0 Divisions rounded towards 0
 *
 * These routines make a division between the two arguments.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x0AE */
sint8 Mfx_Div_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x0C3 */
sint32 Mfx_Div_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup divisionsRoundedOff Divisions rounded off
 *
 * These routines make a division between the two arguments.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x0DE */
sint8 Mfx_RDiv_s8s8_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x0F3 */
sint32 Mfx_RDiv_s32s32_s32(sint32 x_value, sint32 y_value);
/**@}*/

// clang-format off
/**
 * @defgroup multiplicationDivisionRoundedTowards0 Combinations of multiplication and division rounded towards 0
 *
 * These routines make a multiplication between the two arguments and a division
 * by the third argument.
 */
// clang-format on
/**
 * @addtogroup multiplicationDivisionRoundedTowards0
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 * @param z_value Third argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x110 */
sint16 Mfx_MulDiv_s16s16u16_s16(sint16 x_value, sint16 y_value, uint16 z_value);
/** @brief ID: 0x11A */
sint32 Mfx_MulDiv_s32s32s32_s32(sint32 x_value, sint32 y_value, sint32 z_value);
/**@}*/

// clang-format off
/** 
 * @defgroup multiplicationDivisionRoundedOff Combinations of multiplication and division rounded off
 * 
 * These routines make a multiplication between the two arguments and a division
 * by the third argument.
 */
// clang-format on
/**
 * @addtogroup multiplicationDivisionRoundedOff
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 * @param z_value Third argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x12C */
sint16 Mfx_RMulDiv_s16s16s16_s16(sint16 x_value, sint16 y_value,
                                 sint16 z_value);
/** @brief ID: 0x134 */
sint32 Mfx_RMulDiv_s32s32s32_s32(sint32 x_value, sint32 y_value,
                                 sint32 z_value);
/**@}*/

// clang-format off
/** 
 * @defgroup multiplicationShiftRight Combinations of multiplication and shift right
 * 
 * These routines make a multiplication between the two arguments and apply a
 * shift right defined by the third argument.
 */
// clang-format on
/**
 * @addtogroup multiplicationShiftRight
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 * @param shift Third argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x13E. Associated maximum shift: 30. */
sint16 Mfx_MulShRight_s16s16u8_s16(sint16 x_value, sint16 y_value, uint8 shift);
/** @brief ID: 0x14B. Associated maximum shift: 62. */
sint32 Mfx_MulShRight_s32s32u8_s32(sint32 x_value, sint32 y_value, uint8 shift);
/**@}*/

/**
 * @defgroup divisionShiftLeft Combinations of division and shift left
 *
 * These routines apply a shift left defined by the third argument to the first
 * argument, and then make a division by the second argument.
 *
 * @param x_value Numerator.
 * @param y_value Denominator.
 * @param shift Shift left of the fixed point result. Must be a constant
 * expression. Maximum shift according to SWS_Mfx_00064.
 *
 * @return Quotient result.
 */
/**@{*/
/** @brief ID: 0x155. Associated maximum shift: 15. */
sint16 Mfx_DivShLeft_s16s16u8_s16(sint16 x_value, sint16 y_value, uint8 shift);
/** @brief ID: 0x159. Associated maximum shift: 31. */
sint32 Mfx_DivShLeft_s32s32u8_s32(sint32 x_value, sint32 y_value, uint8 shift);
/**@}*/

/**
 * @defgroup modulo Modulo
 *
 * These routines return the remainder of the division @c x_value / @c y_value
 * if @c y_value is not zero.
 *
 * @param x_value First argument.
 * @param y_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x170 */
sint8 Mfx_Mod_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x174 */
sint32 Mfx_Mod_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup limiting Limiting
 *
 * These routines limit the input value between Lower Bound and Upper Bound.
 *
 * @param value Input value.
 * @param min_value Lower Bound. @c min_value shall not be strictly greater than
 * @c max_value.
 * @param max_value Upper Bound. @c max_value shall not be strictly lower than
 * @c min_value.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x17A */
sint8 Mfx_Limit_s8(sint8 value, sint8 min_value, sint8 max_value);
/** @brief ID: 0x17E */
sint32 Mfx_Limit_s32(sint32 value, sint32 min_value, sint32 max_value);
/**@}*/

// clang-format off
/**
 * @defgroup limitationsWithOnlyOneValueForMinimumAndMaximum Limitations with only one value for minimum and maximum
 *
 * These routines limit a value to a minimum or a maximum that depends on the
 * sign of the @c minmax_value.
 */
// clang-format on
/**
 * @addtogroup limitationsWithOnlyOneValueForMinimumAndMaximum
 *
 * @param value First argument.
 * @param minmax_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x180 */
sint8 Mfx_Minmax_s8(sint8 value, sint8 minmax_value);
/** @brief ID: 0x184 */
sint32 Mfx_Minmax_s32(sint32 value, sint32 minmax_value);
/**@}*/

/**
 * @defgroup minimum Minimum
 *
 * These routines return the minimum between two values.
 *
 * @param value First argument.
 * @param minmax_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x186 */
sint8 Mfx_Min_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x18A */
sint32 Mfx_Min_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup maximum Maximum
 *
 * These routines return the maximum between two values.
 *
 * @param value First argument.
 * @param minmax_value Second argument.
 *
 * @return Result of the calculation.
 */
/**@{*/
/** @brief ID: 0x18C */
sint8 Mfx_Max_s8(sint8 x_value, sint8 y_value);
/** @brief ID: 0x190 */
sint32 Mfx_Max_s32(sint32 x_value, sint32 y_value);
/**@}*/

/**
 * @defgroup conversion16bto8b Conversion of 16-bit integer to 8-bit integer
 *
 * These routines convert a scaled 16-bit integer to a scaled 8-bit integer.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the fixed point operand. Must be a constant
 * expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-15 <= (c - a) <= 7</c>
 *
 * @return <c>2 ^ (c - a) * x</c>
 */
/**@{*/
/** @brief ID: 0x191 */
uint8 Mfx_ConvertP2_u16_u8(uint16 x, sint16 a, sint16 c);
/** @brief ID: 0x192 */
sint8 Mfx_ConvertP2_s16_s8(sint16 x, sint16 a, sint16 c);
/**@}*/

/**
 * @defgroup conversion8bto16b Conversion of 8-bit integer to 16-bit integer
 *
 * These routines convert a scaled 8-bit integer to a scaled 16-bit integer.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the fixed point operand. Must be a constant
 * expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-7 <= (c - a) <= 15</c>
 *
 * @return <c>2 ^ (c - a) * x</c>
 */
/**@{*/
/** @brief ID: 0x193 */
uint16 Mfx_ConvertP2_u8_u16(uint8 x, sint16 a, sint16 c);
/** @brief ID: 0x194 */
sint16 Mfx_ConvertP2_s8_s16(sint8 x, sint16 a, sint16 c);
/**@}*/

/**
 * @defgroup conversion32bto16b Conversion of 32-bit integer to 16-bit integer
 *
 * These routines convert a scaled 32-bit integer to a scaled 16-bit integer.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the fixed point operand. Must be a constant
 * expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-31 <= (c - a) <= 15</c>
 *
 * @return <c>2 ^ (c - a) * x</c>
 */
/**@{*/
/** @brief ID: 0x195 */
uint16 Mfx_ConvertP2_u32_u16(uint32 x, sint16 a, sint16 c);
/** @brief ID: 0x196 */
sint16 Mfx_ConvertP2_s32_s16(sint32 x, sint16 a, sint16 c);
/**@}*/

/**
 * @defgroup conversion16bto32b Conversion of 16-bit integer to 32-bit integer
 *
 * These routines convert a scaled 16-bit integer to a scaled 32-bit integer.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the fixed point operand. Must be a constant
 * expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-15 <= (c - a) <= 31</c>
 *
 * @return <c>2 ^ (c - a) * x</c>
 */
/**@{*/
/** @brief ID: 0x197 */
uint32 Mfx_ConvertP2_u16_u32(uint16 x, sint16 a, sint16 c);
/** @brief ID: 0x198 */
sint32 Mfx_ConvertP2_s16_s32(sint16 x, sint16 a, sint16 c);
/**@}*/

/**
 * @defgroup multiplication2n16b 16-Bit Multiplication of 2n Scaled Integer
 *
 * These routines multiply two 16-bit integers with scaling factors set by
 * input parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-31 <= (c - b - a) <= 15</c>
 *
 * @return <c>2 ^ (c - b - a) * [x * y]</c>
 */
/**@{*/
/** @brief ID: 0x199 */
uint16 Mfx_MulP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x19E */
sint16 Mfx_MulP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup multiplication2n32b 32-Bit Multiplication of 2n Scaled Integer
 *
 * These routines multiply two 32-bit integers with scaling factors set by
 * input parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-63 <= (c - b - a) <= 31</c>
 *
 * @return <c>2 ^ (c - b - a) * [x * y]</c>
 */
/**@{*/
/** @brief ID: 0x19F */
uint32 Mfx_MulP2_u32u32_u32(uint32 x, uint32 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x1A4 */
sint32 Mfx_MulP2_s32s32_s32(sint32 x, sint32 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup division2n16b 16-Bit Division of 2n Scaled Integer
 *
 * These routines divide two 16-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-15 <= (c + b - a) <= 31</c>
 *
 * @return <c>[2 ^ (c + b - a) * x] / y</c>
 */
/**@{*/
/** @brief ID: 0x1A5 */
uint16 Mfx_DivP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x1AC */
sint16 Mfx_DivP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup division2n32b 32-Bit Division of 2n Scaled Integer
 *
 * These routines divide two 32-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-31 <= (c + b - a) <= 63</c>
 *
 * @return <c>[2 ^ (c + b - a) * x] / y</c>
 */
/**@{*/
/** @brief ID: 0x1AD */
uint32 Mfx_DivP2_u32u32_u32(uint32 x, uint32 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x1B4 */
sint32 Mfx_DivP2_s32s32_s32(sint32 x, sint32 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup addition2n16b 16-Bit Addition of 2n Scaled Integer
 *
 * These routines add two 16-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range:
 *   - <c>0 <= |a - b| <= 15</c>
 *   - <c>(c - b) <= 15</c>, <c>(a - c) <= 15</c> if <c>a >= b</c>
 *   - <c>(c - a) <= 15</c>, <c>(b - c) <= 15</c> if <c>a < b</c>
 *
 * @return
 *   - <c>2 ^ (c - a) * [x + (y * 2 ^ (a - b))]</c> if <c>a >= b</c>
 *   - <c>2 ^ (c - b) * [(x * 2 ^ (b - a)) + y]</c> if <c>a < b</c>
 */
/**@{*/
/** @brief ID: 0x1B5 */
uint16 Mfx_AddP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x1BA */
sint16 Mfx_AddP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup addition2n32b 32-Bit Addition of 2n Scaled Integer
 *
 * These routines add two 32-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range:
 *   - <c>0 <= |a - b| <= 15</c>
 *   - <c>(c - b) <= 15</c>, <c>(a - c) <= 15</c> if <c>a >= b</c>
 *   - <c>(c - a) <= 15</c>, <c>(b - c) <= 15</c> if <c>a < b</c>
 *
 * @return
 *   - <c>2 ^ (c - a) * [x + (y * 2 ^ (a - b))]</c> if <c>a >= b</c>
 *   - <c>2 ^ (c - b) * [(x * 2 ^ (b - a)) + y]</c> if <c>a < b</c>
 */
/**@{*/
/** @brief ID: 0x1BB */
uint32 Mfx_AddP2_u32u32_u32(uint32 x, uint32 y, sint32 a, sint32 b, sint32 c);
/** @brief ID: 0x1C0 */
sint32 Mfx_AddP2_s32s32_s32(sint32 x, sint32 y, sint32 a, sint32 b, sint32 c);
/**@}*/

/**
 * @defgroup subtraction2n16b 16-Bit Subtraction of 2n Scaled Integer
 *
 * These routines subtract two 16-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range:
 *   - <c>0 <= |a - b| <= 15</c>
 *   - <c>(c - b) <= 15</c>, <c>(a - c) <= 15</c> if <c>a >= b</c>
 *   - <c>(c - a) <= 15</c>, <c>(b - c) <= 15</c> if <c>a < b</c>
 *
 * @return
 *   - <c>2 ^ (c - a) * [x - (y * 2 ^ (a - b))]</c> if <c>a >= b</c>
 *   - <c>2 ^ (c - b) * [(x * 2 ^ (b - a)) - y]</c> if <c>a < b</c>
 */
/**@{*/
/** @brief ID: 0x1C1 */
uint16 Mfx_SubP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c);
/** @brief ID: 0x1C8 */
sint16 Mfx_SubP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c);
/**@}*/

/**
 * @defgroup subtraction2n32b 32-Bit Subtraction of 2n Scaled Integer
 *
 * These routines subtract two 32-bit integers with scaling factors set by input
 * parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range:
 *   - <c>0 <= |a - b| <= 15</c>
 *   - <c>(c - b) <= 15</c>, <c>(a - c) <= 15</c> if <c>a >= b</c>
 *   - <c>(c - a) <= 15</c>, <c>(b - c) <= 15</c> if <c>a < b</c>
 *
 * @return
 *   - <c>2 ^ (c - a) * [x - (y * 2 ^ (a - b))]</c> if <c>a >= b</c>
 *   - <c>2 ^ (c - b) * [(x * 2 ^ (b - a)) - y]</c> if <c>a < b</c>
 */
/**@{*/
/** @brief ID: 0x1C9 */
uint32 Mfx_SubP2_u32u32_u32(uint32 x, uint32 y, sint32 a, sint32 b, sint32 c);
/** @brief ID: 0x1D0 */
sint32 Mfx_SubP2_s32s32_s32(sint32 x, sint32 y, sint32 a, sint32 b, sint32 c);
/**@}*/

/**
 * @defgroup absDiff2nScaled Absolute Difference of 2n Scaled Integer
 *
 * These routines subtract and take the absolute value of two 16-bit integers
 * with scaling factors set by input parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param y Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param b Radix point position of the second fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range:
 *   - <c>0 <= |a - b| <= 15</c>
 *   - <c>(c - b) <= 15</c>, <c>(a - c) <= 15</c> if <c>a >= b</c>
 *   - <c>(c - a) <= 15</c>, <c>(b - c) <= 15</c> if <c>a < b</c>
 *
 * @return
 *   - <c>2 ^ (c - a) * |x - (y * 2 ^ (a - b))|</c> if <c>a >= b</c>
 *   - <c>2 ^ (c - b) * |(x * 2 ^ (b - a)) - y|</c> if <c>a < b</c>
 */
/**@{*/
/** @brief ID: 0x1D1 */
uint16 Mfx_AbsDiffP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b,
                                sint16 c);
/** @brief ID: 0x1D6 */
sint16 Mfx_AbsDiffP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b,
                                sint16 c);
/**@}*/

/**
 * @defgroup abs2nScaled16b 16-Bit Absolute Value of 2n Scaled Integer
 *
 * These routines take the absolute value of a 16-bit integer with scaling
 * factors set by input parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-15 <= (c - a) <= 15</c>
 *
 * @return <c>2 ^ (c - a) * |x|</c>
 */
/**@{*/
/** @brief ID: 0x1D7 */
uint16 Mfx_AbsP2_s16_u16(sint16 x, sint16 a, sint16 c);
/** @brief ID: 0x1D8 */
sint16 Mfx_AbsP2_s16_s16(sint16 x, sint16 a, sint16 c);
/**@}*/

/**
 * @defgroup abs2nScaled32b 32-Bit Absolute Value of 2n Scaled Integer
 *
 * These routines take the absolute value of a 32-bit integer with scaling
 * factors set by input parameters.
 *
 * @param x Integer value of the fixed-point operand.
 * @param a Radix point position of the first fixed point operand. Must be a
 * constant expression.
 * @param c Radix point position of the fixed point result. Must be a constant
 * expression.
 *
 * @note
 * Valid range: <c>-31 <= (c - a) <= 31</c>
 *
 * @return <c>2 ^ (c - a) * |x|</c>
 */
/**@{*/
/** @brief ID: 0x1D9 */
uint32 Mfx_AbsP2_s32_u32(sint32 x, sint16 a, sint16 c);
/** @brief ID: 0x1DA */
sint32 Mfx_AbsP2_s32_s32(sint32 x, sint16 a, sint16 c);
/**@}*/

#endif // MFX_H
