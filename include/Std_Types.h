#ifndef STD_TYPES_H
#define STD_TYPES_H

// Partial Std_Types.h implementation.

#include "Platform_Types.h"

typedef uint8 Std_ReturnType;

typedef struct {
    uint16 vendorID;
    uint16 moduleID;
    uint8 sw_major_version;
    uint8 sw_minor_version;
    uint8 sw_patch_version;
} Std_VersionInfoType;

#ifndef STATUSTYPEDEFINED
#    define STATUSTYPEDEFINED
#    define E_OK 0x00U
#endif
#define E_NOT_OK 0x01U

#define STD_HIGH 0x01U /* Physical state 5V or 3.3V */
#define STD_LOW 0x00U  /* Physical state 0V */

#define STD_ACTIVE 0x01U /* Logical state active */
#define STD_IDLE 0x00U   /* Logical state idle */

#define STD_ON 0x01U
#define STD_OFF 0x00U

#endif /* STD_TYPES_H */
