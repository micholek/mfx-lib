# MFX partial implementation

The project contains partial implementation of [Specification of Fixed Point Math Routines AUTOSAR CP R21-11](https://autosar.org/fileadmin/user_upload/standards/classic/21-11/AUTOSAR_SWS_MFXLibrary.pdf).

## Tools

 - Compiler: GCC
 - Coverage: [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html),
   [gcovr](https://gcovr.com/en/stable/index.html) (optional for generating
   coverage report)
 - Unit tests: [Acutest](https://github.com/mity/acutest)
 - Fake functions/mocks: [fff](https://github.com/meekrosoft/fff)
 - Static analysis: [Cppcheck](https://github.com/danmar/cppcheck)
 - Documentation (optional): [Doxygen](https://doxygen.nl)

## Building and running tests

The provided Makefile allows for building the source files, running tests and
coverage.
 - `make` or `make all` - runs static analysis, tests and coverage
 - `make report` - generates coverage report that can be found in `build/report`
   directory
 - `make doc` - generates documentation that can be found in `doc` directory
 - `make clean` - cleans workspace
