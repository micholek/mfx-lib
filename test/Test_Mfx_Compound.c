#include "acutest.h"
#include "fff.h"

#include "Mfx.h"

DEFINE_FFF_GLOBALS;

// GCOV_EXCL_START

FAKE_VALUE_FUNC(sint8, Mfx_Abs_s8_s8, sint8);
FAKE_VALUE_FUNC(sint32, Mfx_Abs_s32_s32, sint32);

FAKE_VALUE_FUNC(sint8, Mfx_Sub_s8s8_s8, sint8, sint8);
FAKE_VALUE_FUNC(sint32, Mfx_Sub_s32s32_s32, sint32, sint32);

// GCOV_EXCL_STOP

void test_AbsDiff_8(void) {
    sint8 result;

    Mfx_Sub_s8s8_s8_fake.return_val = -MFX_SINT8_MAX;
    Mfx_Abs_s8_s8_fake.return_val = MFX_SINT8_MAX;
    result = Mfx_AbsDiff_s8s8_s8(0, MFX_SINT8_MAX);
    TEST_CHECK(result == MFX_SINT8_MAX);

    Mfx_Sub_s8s8_s8_fake.return_val = MFX_SINT8_MIN;
    Mfx_Abs_s8_s8_fake.return_val = MFX_SINT8_MAX;
    result = Mfx_AbsDiff_s8s8_s8(0, MFX_SINT8_MAX);
    TEST_CHECK(result == MFX_SINT8_MAX);

    Mfx_Sub_s8s8_s8_fake.return_val = -29;
    Mfx_Abs_s8_s8_fake.return_val = 29;
    result = Mfx_AbsDiff_s8s8_s8(15, 44);
    TEST_CHECK(result == 29);
}

void test_AbsDiff_32(void) {
    sint32 result;

    Mfx_Sub_s32s32_s32_fake.return_val = -MFX_SINT32_MAX;
    Mfx_Abs_s32_s32_fake.return_val = MFX_SINT32_MAX;
    result = Mfx_AbsDiff_s32s32_s32(0L, MFX_SINT32_MAX);
    TEST_CHECK(result == MFX_SINT32_MAX);

    Mfx_Sub_s32s32_s32_fake.return_val = MFX_SINT32_MIN;
    Mfx_Abs_s32_s32_fake.return_val = MFX_SINT32_MAX;
    result = Mfx_AbsDiff_s32s32_s32(0L, MFX_SINT32_MAX);
    TEST_CHECK(result == MFX_SINT32_MAX);

    Mfx_Sub_s32s32_s32_fake.return_val = -1153493L;
    Mfx_Abs_s32_s32_fake.return_val = 1153493L;
    result = Mfx_AbsDiff_s32s32_s32(-311098L, 842395L);
    TEST_CHECK(result == 1153493L);
}

TEST_LIST = {{"AbsDiff_8", test_AbsDiff_8},
             {"AbsDiff_32", test_AbsDiff_32},
             {NULL, NULL}};
