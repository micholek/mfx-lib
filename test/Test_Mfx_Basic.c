#include "acutest.h"

#include "Mfx.h"

void test_GetVersionInfo(void) {
    Std_VersionInfoType version_info;

    Mfx_GetVersionInfo(&version_info);
    TEST_CHECK(version_info.moduleID == MFX_MODULE_ID);
    TEST_CHECK(version_info.vendorID == MFX_VENDOR_ID);
    TEST_CHECK(version_info.sw_major_version == MFX_SW_MAJOR_VERSION);
    TEST_CHECK(version_info.sw_minor_version == MFX_SW_MINOR_VERSION);
    TEST_CHECK(version_info.sw_patch_version == MFX_SW_PATCH_VERSION);
}

void test_Add_8(void) {
    sint8 result;

    result = Mfx_Add_s8s8_s8(-35, 58);
    TEST_CHECK(result == 23);

    result = Mfx_Add_s8s8_s8(100, 100);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_Add_s8s8_s8(-50, -120);
    TEST_CHECK(result == MFX_SINT8_MIN);
}

void test_Add_32(void) {
    sint32 result;

    result = Mfx_Add_s32s32_s32(-1337L, 21377L);
    TEST_CHECK(result == 20040L);

    result = Mfx_Add_s32s32_s32(2000000000L, 2000000000L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_Add_s32s32_s32(-2000000000L, -2000000000L);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_Sub_8(void) {
    sint8 result;

    result = Mfx_Sub_s8s8_s8(35, 58);
    TEST_CHECK(result == -23);

    result = Mfx_Sub_s8s8_s8(-100, 100);
    TEST_CHECK(result == MFX_SINT8_MIN);

    result = Mfx_Sub_s8s8_s8(50, -120);
    TEST_CHECK(result == MFX_SINT8_MAX);
}

void test_Sub_32(void) {
    sint32 result;

    result = Mfx_Sub_s32s32_s32(4141L, 58097L);
    TEST_CHECK(result == -53956L);

    result = Mfx_Sub_s32s32_s32(-2000000000L, 2000000000L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_Sub_s32s32_s32(2000000000L, -2000000000L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_Abs_8(void) {
    sint8 result;

    result = Mfx_Abs_s8_s8(MFX_SINT8_MIN);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_Abs_s8_s8(MFX_SINT8_MAX);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_Abs_s8_s8(-33);
    TEST_CHECK(result == 33);

    result = Mfx_Abs_s8_s8(123);
    TEST_CHECK(result == 123);
}

void test_Abs_32(void) {
    sint32 result;

    result = Mfx_Abs_s32_s32(MFX_SINT32_MIN);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_Abs_s32_s32(MFX_SINT32_MAX);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_Abs_s32_s32(-49994235L);
    TEST_CHECK(result == 49994235L);

    result = Mfx_Abs_s32_s32(31231545L);
    TEST_CHECK(result == 31231545L);
}

void test_Mul_8(void) {
    sint8 result;

    result = Mfx_Mul_s8s8_s8(4, -17);
    TEST_CHECK(result == -68);

    result = Mfx_Mul_s8s8_s8(-66, 66);
    TEST_CHECK(result == MFX_SINT8_MIN);

    result = Mfx_Mul_s8s8_s8(-14, -55);
    TEST_CHECK(result == MFX_SINT8_MAX);
}

void test_Mul_32(void) {
    sint32 result;

    result = Mfx_Mul_s32s32_s32(1244L, -67L);
    TEST_CHECK(result == -83348L);

    result = Mfx_Mul_s32s32_s32(-5555555L, 5555555L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_Mul_s32s32_s32(-5555555L, -5555555L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_Div_8(void) {
    sint8 result;

    result = Mfx_Div_s8s8_s8(99, -3);
    TEST_CHECK(result == -33);

    result = Mfx_Div_s8s8_s8(-56, 0);
    TEST_CHECK(result == MFX_SINT8_MIN);

    result = Mfx_Div_s8s8_s8(0, 0);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_Div_s8s8_s8(74, 0);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_Div_s8s8_s8(MFX_SINT8_MIN, -1);
    TEST_CHECK(result == MFX_SINT8_MAX);
}

void test_Div_32(void) {
    sint32 result;

    result = Mfx_Div_s32s32_s32(999999L, -3L);
    TEST_CHECK(result == -333333L);

    result = Mfx_Div_s32s32_s32(-123456L, 0L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_Div_s32s32_s32(0L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_Div_s32s32_s32(66666L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_Div_s32s32_s32(MFX_SINT32_MIN, -1L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_RDiv_8(void) {
    sint8 result;

    result = Mfx_RDiv_s8s8_s8(5, 2);
    TEST_CHECK(result == 3);

    result = Mfx_RDiv_s8s8_s8(99, -3);
    TEST_CHECK(result == -33);

    result = Mfx_RDiv_s8s8_s8(3, 99);
    TEST_CHECK(result == 0);

    result = Mfx_RDiv_s8s8_s8(99, -4);
    TEST_CHECK(result == -25);

    result = Mfx_RDiv_s8s8_s8(99, -7);
    TEST_CHECK(result == -14);

    result = Mfx_RDiv_s8s8_s8(99, 4);
    TEST_CHECK(result == 25);

    result = Mfx_RDiv_s8s8_s8(99, 7);
    TEST_CHECK(result == 14);

    result = Mfx_RDiv_s8s8_s8(-55, 0);
    TEST_CHECK(result == MFX_SINT8_MIN);

    result = Mfx_RDiv_s8s8_s8(0, 0);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_RDiv_s8s8_s8(77, 0);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_RDiv_s8s8_s8(MFX_SINT8_MIN, -1);
    TEST_CHECK(result == MFX_SINT8_MAX);
}

void test_RDiv_32(void) {
    sint32 result;

    result = Mfx_RDiv_s32s32_s32(5L, 2L);
    TEST_CHECK(result == 3L);

    result = Mfx_RDiv_s32s32_s32(999999L, -3L);
    TEST_CHECK(result == -333333L);

    result = Mfx_RDiv_s32s32_s32(3L, 999999L);
    TEST_CHECK(result == 0L);

    result = Mfx_RDiv_s32s32_s32(999999L, -4L);
    TEST_CHECK(result == -250000L);

    result = Mfx_RDiv_s32s32_s32(999999L, -12L);
    TEST_CHECK(result == -83333L);

    result = Mfx_RDiv_s32s32_s32(999999L, 4L);
    TEST_CHECK(result == 250000L);

    result = Mfx_RDiv_s32s32_s32(999999L, 12L);
    TEST_CHECK(result == 83333L);

    result = Mfx_RDiv_s32s32_s32(-654321L, 0L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_RDiv_s32s32_s32(0L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_RDiv_s32s32_s32(999999L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_RDiv_s32s32_s32(MFX_SINT32_MIN, -1L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_MulDiv_16(void) {
    sint16 result;

    result = Mfx_MulDiv_s16s16u16_s16(2, 10, 7);
    TEST_CHECK(result == 2);

    result = Mfx_MulDiv_s16s16u16_s16(2, 10, 6);
    TEST_CHECK(result == 3);

    result = Mfx_MulDiv_s16s16u16_s16(400, 200, 8);
    TEST_CHECK(result == 10000);

    result = Mfx_MulDiv_s16s16u16_s16(-400, 200, 8);
    TEST_CHECK(result == -10000);

    result = Mfx_MulDiv_s16s16u16_s16(-800, -200, 8);
    TEST_CHECK(result == 20000);

    result = Mfx_MulDiv_s16s16u16_s16(0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_MulDiv_s16s16u16_s16(-50, -100, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_MulDiv_s16s16u16_s16(50, -100, 0);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_MulDiv_s16s16u16_s16(50, -1000, 1);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_MulDiv_s16s16u16_s16(50, 1000, 1);
    TEST_CHECK(result == MFX_SINT16_MAX);
}

void test_MulDiv_32(void) {
    sint32 result;

    result = Mfx_MulDiv_s32s32s32_s32(2L, 10L, 7L);
    TEST_CHECK(result == 2L);

    result = Mfx_MulDiv_s32s32s32_s32(2L, 10L, -7L);
    TEST_CHECK(result == -2L);

    result = Mfx_MulDiv_s32s32s32_s32(2L, 10L, 6L);
    TEST_CHECK(result == 3L);

    result = Mfx_MulDiv_s32s32s32_s32(4000L, 200L, 8L);
    TEST_CHECK(result == 100000L);

    result = Mfx_MulDiv_s32s32s32_s32(-4000L, 200L, 8L);
    TEST_CHECK(result == -100000L);

    result = Mfx_MulDiv_s32s32s32_s32(-8000L, -200L, 8L);
    TEST_CHECK(result == 200000L);

    result = Mfx_MulDiv_s32s32s32_s32(0L, 0L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulDiv_s32s32s32_s32(-500L, -100L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulDiv_s32s32s32_s32(500L, -100L, 0L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_MulDiv_s32s32s32_s32(30L, -100000000L, 1L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_MulDiv_s32s32s32_s32(30L, 100000000L, 1L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_RMulDiv_16(void) {
    sint16 result;

    result = Mfx_RMulDiv_s16s16s16_s16(2, 10, 7);
    TEST_CHECK(result == 3);

    result = Mfx_RMulDiv_s16s16s16_s16(2, 10, -7);
    TEST_CHECK(result == -3);

    result = Mfx_RMulDiv_s16s16s16_s16(2, 10, 6);
    TEST_CHECK(result == 3);

    result = Mfx_RMulDiv_s16s16s16_s16(400, 200, 8);
    TEST_CHECK(result == 10000);

    result = Mfx_RMulDiv_s16s16s16_s16(-400, 200, 8);
    TEST_CHECK(result == -10000);

    result = Mfx_RMulDiv_s16s16s16_s16(-800, -200, 8);
    TEST_CHECK(result == 20000);

    result = Mfx_RMulDiv_s16s16s16_s16(0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_RMulDiv_s16s16s16_s16(-50, -100, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_RMulDiv_s16s16s16_s16(50, -100, 0);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_RMulDiv_s16s16s16_s16(50, -1000, 1);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_RMulDiv_s16s16s16_s16(50, 1000, 1);
    TEST_CHECK(result == MFX_SINT16_MAX);
}

void test_RMulDiv_32(void) {
    sint32 result;

    result = Mfx_RMulDiv_s32s32s32_s32(2L, 10L, 7L);
    TEST_CHECK(result == 3L);

    result = Mfx_RMulDiv_s32s32s32_s32(2L, 10L, -7L);
    TEST_CHECK(result == -3L);

    result = Mfx_RMulDiv_s32s32s32_s32(2L, 10L, 6L);
    TEST_CHECK(result == 3L);

    result = Mfx_RMulDiv_s32s32s32_s32(4000L, 200L, 8L);
    TEST_CHECK(result == 100000L);

    result = Mfx_RMulDiv_s32s32s32_s32(-4000L, 200L, 8L);
    TEST_CHECK(result == -100000);

    result = Mfx_RMulDiv_s32s32s32_s32(-8000L, -200L, 8L);
    TEST_CHECK(result == 200000L);

    result = Mfx_RMulDiv_s32s32s32_s32(0L, 0L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_RMulDiv_s32s32s32_s32(-500L, -100L, 0L);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_RMulDiv_s32s32s32_s32(500L, -100L, 0L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_RMulDiv_s32s32s32_s32(30L, -100000000L, 1L);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_RMulDiv_s32s32s32_s32(30L, 100000000L, 1L);
    TEST_CHECK(result == MFX_SINT32_MAX);
}

void test_MulShRight_16(void) {
    sint16 result;

    result = Mfx_MulShRight_s16s16u8_s16(4, 50, 2);
    TEST_CHECK(result == 50);

    result = Mfx_MulShRight_s16s16u8_s16(-4, 50, 2);
    TEST_CHECK(result == -50);

    result = Mfx_MulShRight_s16s16u8_s16(15, 10, 30);
    TEST_CHECK(result == 0);

    result = Mfx_MulShRight_s16s16u8_s16(MFX_SINT16_MAX, MFX_SINT16_MAX, 30);
    TEST_CHECK(result == 0);

    result = Mfx_MulShRight_s16s16u8_s16(MFX_SINT16_MIN, MFX_SINT16_MAX, 31);
    TEST_CHECK(result == -1);

    result = Mfx_MulShRight_s16s16u8_s16(MFX_SINT16_MAX, MFX_SINT16_MAX, 1);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_MulShRight_s16s16u8_s16(MFX_SINT16_MIN, MFX_SINT16_MAX, 1);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_MulShRight_32(void) {
    sint32 result;

    result = Mfx_MulShRight_s32s32u8_s32(4L, 50L, 2);
    TEST_CHECK(result == 50L);

    result = Mfx_MulShRight_s32s32u8_s32(-4L, 50L, 2);
    TEST_CHECK(result == -50);

    result = Mfx_MulShRight_s32s32u8_s32(15L, 10L, 30);
    TEST_CHECK(result == 0L);

    result = Mfx_MulShRight_s32s32u8_s32(MFX_SINT32_MAX, MFX_SINT32_MAX, 62);
    TEST_CHECK(result == 0L);

    result = Mfx_MulShRight_s32s32u8_s32(MFX_SINT32_MIN, MFX_SINT32_MAX, 63);
    TEST_CHECK(result == -1L);

    result = Mfx_MulShRight_s32s32u8_s32(MFX_SINT32_MAX, MFX_SINT32_MAX, 1);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulShRight_s32s32u8_s32(MFX_SINT32_MIN, MFX_SINT32_MAX, 1);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_DivShLeft_16(void) {
    sint16 result;

    result = Mfx_DivShLeft_s16s16u8_s16(4000, 16, 2);
    TEST_CHECK(result == 1000);

    result = Mfx_DivShLeft_s16s16u8_s16(-4000, 16, 2);
    TEST_CHECK(result == -1000);

    result = Mfx_DivShLeft_s16s16u8_s16(20, 0, 6);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_DivShLeft_s16s16u8_s16(-20, 0, 6);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_DivShLeft_s16s16u8_s16(20, 1, 15);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_DivShLeft_s16s16u8_s16(-20, 1, 15);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_DivShLeft_32(void) {
    sint32 result;

    result = Mfx_DivShLeft_s32s32u8_s32(400000L, 16L, 2);
    TEST_CHECK(result == 100000L);

    result = Mfx_DivShLeft_s32s32u8_s32(-400000L, 16L, 2);
    TEST_CHECK(result == -100000L);

    result = Mfx_DivShLeft_s32s32u8_s32(20L, 0L, 6);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivShLeft_s32s32u8_s32(-20L, 0L, 6);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_DivShLeft_s32s32u8_s32(200L, 1L, 31);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivShLeft_s32s32u8_s32(-200L, 1L, 31);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_Mod_8(void) {
    sint8 result;

    result = Mfx_Mod_s8(64, 9);
    TEST_CHECK(result == 1);

    result = Mfx_Mod_s8(64, -9);
    TEST_CHECK(result == 1);

    result = Mfx_Mod_s8(-64, 9);
    TEST_CHECK(result == -1);

    result = Mfx_Mod_s8(-64, -9);
    TEST_CHECK(result == -1);

    result = Mfx_Mod_s8(-66, 0);
    TEST_CHECK(result == 0);

    result = Mfx_Mod_s8(MFX_SINT8_MIN, -2);
    TEST_CHECK(result == 0);

    result = Mfx_Mod_s8(MFX_SINT8_MIN, -1);
    TEST_CHECK(result == 0);
}

void test_Mod_32(void) {
    sint32 result;

    result = Mfx_Mod_s32(6454645L, 99L);
    TEST_CHECK(result == 43L);

    result = Mfx_Mod_s32(6454645L, -99L);
    TEST_CHECK(result == 43L);

    result = Mfx_Mod_s32(-6454645L, 99L);
    TEST_CHECK(result == -43L);

    result = Mfx_Mod_s32(-6454645L, -99L);
    TEST_CHECK(result == -43L);

    result = Mfx_Mod_s32(-6454645L, 0L);
    TEST_CHECK(result == 0L);

    result = Mfx_Mod_s32(MFX_SINT32_MIN, -2L);
    TEST_CHECK(result == 0L);

    result = Mfx_Mod_s32(MFX_SINT32_MIN, -1L);
    TEST_CHECK(result == 0L);
}

void test_Limit_8(void) {
    sint8 result;

    result = Mfx_Limit_s8(64, 10, 80);
    TEST_CHECK(result == 64);

    result = Mfx_Limit_s8(64, 10, 60);
    TEST_CHECK(result == 60);

    result = Mfx_Limit_s8(64, 80, 120);
    TEST_CHECK(result == 80);
}

void test_Limit_32(void) {
    sint32 result;

    result = Mfx_Limit_s32(64000L, 10000L, 80000L);
    TEST_CHECK(result == 64000L);

    result = Mfx_Limit_s32(64000L, 10000L, 60000L);
    TEST_CHECK(result == 60000L);

    result = Mfx_Limit_s32(64000L, 80000L, 120000L);
    TEST_CHECK(result == 80000L);
}

void test_Minmax_8(void) {
    sint8 result;

    result = Mfx_Minmax_s8(64, 10);
    TEST_CHECK(result == 10);

    result = Mfx_Minmax_s8(64, -10);
    TEST_CHECK(result == 64);

    result = Mfx_Minmax_s8(-64, 10);
    TEST_CHECK(result == -64);

    result = Mfx_Minmax_s8(-64, -10);
    TEST_CHECK(result == -10);
}

void test_Minmax_32(void) {
    sint32 result;

    result = Mfx_Minmax_s32(64000L, 10000L);
    TEST_CHECK(result == 10000L);

    result = Mfx_Minmax_s32(64000L, -10000L);
    TEST_CHECK(result == 64000L);

    result = Mfx_Minmax_s32(-64000L, 10000L);
    TEST_CHECK(result == -64000L);

    result = Mfx_Minmax_s32(-64000L, -10000L);
    TEST_CHECK(result == -10000L);
}

void test_Min_8(void) {
    sint8 result;

    result = Mfx_Min_s8(64, 10);
    TEST_CHECK(result == 10);

    result = Mfx_Min_s8(-64, 10);
    TEST_CHECK(result == -64);
}

void test_Min_32(void) {
    sint32 result;

    result = Mfx_Min_s32(64000L, 10000L);
    TEST_CHECK(result == 10000L);

    result = Mfx_Min_s32(-64000L, 10000L);
    TEST_CHECK(result == -64000L);
}

void test_Max_8(void) {
    sint8 result;

    result = Mfx_Max_s8(64, 10);
    TEST_CHECK(result == 64);

    result = Mfx_Max_s8(-64, 10);
    TEST_CHECK(result == 10);
}

void test_Max_32(void) {
    sint32 result;

    result = Mfx_Max_s32(64000L, 10000L);
    TEST_CHECK(result == 64000L);

    result = Mfx_Max_s32(-64000L, 10000L);
    TEST_CHECK(result == 10000L);
}

void test_ConvertP2_u16_u8(void) {
    uint8 result;

    result = Mfx_ConvertP2_u16_u8(32, 3, 5);
    TEST_CHECK(result == 128);

    result = Mfx_ConvertP2_u16_u8(32, 5, 3);
    TEST_CHECK(result == 8);

    result = Mfx_ConvertP2_u16_u8(32, 3, 50);
    TEST_CHECK(result == MFX_UINT8_MAX);

    result = Mfx_ConvertP2_u16_u8(32, 50, 3);
    TEST_CHECK(result == MFX_UINT8_MIN);

    result = Mfx_ConvertP2_u16_u8(MFX_UINT16_MAX, 5, 10);
    TEST_CHECK(result == MFX_UINT8_MAX);

    result = Mfx_ConvertP2_u16_u8(MFX_UINT16_MIN, 10, 5);
    TEST_CHECK(result == MFX_UINT8_MIN);
}

void test_ConvertP2_s16_s8(void) {
    sint8 result;

    result = Mfx_ConvertP2_s16_s8(32, 4, 5);
    TEST_CHECK(result == 64);

    result = Mfx_ConvertP2_s16_s8(32, 5, 4);
    TEST_CHECK(result == 16);

    result = Mfx_ConvertP2_s16_s8(32, 4, 60);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_ConvertP2_s16_s8(32, 60, 4);
    TEST_CHECK(result == MFX_SINT8_MIN);

    result = Mfx_ConvertP2_s16_s8(MFX_SINT16_MAX, 5, 10);
    TEST_CHECK(result == MFX_SINT8_MAX);

    result = Mfx_ConvertP2_s16_s8(MFX_SINT16_MIN, 5, 10);
    TEST_CHECK(result == MFX_SINT8_MIN);
}

void test_ConvertP2_u8_u16(void) {
    uint16 result;

    result = Mfx_ConvertP2_u8_u16(32, 3, 5);
    TEST_CHECK(result == 128);

    result = Mfx_ConvertP2_u8_u16(32, 5, 3);
    TEST_CHECK(result == 8);

    result = Mfx_ConvertP2_u8_u16(32, 3, 50);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_ConvertP2_u8_u16(32, 50, 3);
    TEST_CHECK(result == MFX_UINT16_MIN);

    result = Mfx_ConvertP2_u8_u16(MFX_UINT8_MAX, 5, 17);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_ConvertP2_u8_u16(MFX_UINT8_MIN, 5, 3);
    TEST_CHECK(result == MFX_UINT16_MIN);
}

void test_ConvertP2_s8_s16(void) {
    sint16 result;

    result = Mfx_ConvertP2_s8_s16(32, 4, 5);
    TEST_CHECK(result == 64);

    result = Mfx_ConvertP2_s8_s16(32, 5, 4);
    TEST_CHECK(result == 16);

    result = Mfx_ConvertP2_s8_s16(32, 4, 60);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_ConvertP2_s8_s16(32, 60, 4);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_ConvertP2_s8_s16(MFX_SINT8_MAX, 2, 17);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_ConvertP2_s8_s16(MFX_SINT8_MIN, 2, 17);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_ConvertP2_u32_u16(void) {
    uint16 result;

    result = Mfx_ConvertP2_u32_u16(32L, 3, 5);
    TEST_CHECK(result == 128);

    result = Mfx_ConvertP2_u32_u16(32L, 5, 3);
    TEST_CHECK(result == 8);

    result = Mfx_ConvertP2_u32_u16(32L, 3, 50);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_ConvertP2_u32_u16(32L, 50, 3);
    TEST_CHECK(result == MFX_UINT16_MIN);

    result = Mfx_ConvertP2_u32_u16(MFX_UINT32_MAX, 0, 15);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_ConvertP2_u32_u16(MFX_UINT32_MIN, 31, 0);
    TEST_CHECK(result == MFX_UINT16_MIN);
}

void test_ConvertP2_s32_s16(void) {
    sint16 result;

    result = Mfx_ConvertP2_s32_s16(32L, 4, 5);
    TEST_CHECK(result == 64);

    result = Mfx_ConvertP2_s32_s16(32L, 5, 4);
    TEST_CHECK(result == 16);

    result = Mfx_ConvertP2_s32_s16(32L, 4, 60);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_ConvertP2_s32_s16(32L, 60, 4);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_ConvertP2_s32_s16(MFX_SINT32_MAX, 0, 15);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_ConvertP2_s32_s16(MFX_SINT32_MIN, 0, 15);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_ConvertP2_u16_u32(void) {
    uint32 result;

    result = Mfx_ConvertP2_u16_u32(32, 3, 5);
    TEST_CHECK(result == 128L);

    result = Mfx_ConvertP2_u16_u32(32, 5, 3);
    TEST_CHECK(result == 8L);

    result = Mfx_ConvertP2_u16_u32(32, 3, 50);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_ConvertP2_u16_u32(32, 50, 3);
    TEST_CHECK(result == MFX_UINT32_MIN);

    result = Mfx_ConvertP2_u16_u32(MFX_UINT16_MAX, 0, 31);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_ConvertP2_u16_u32(MFX_UINT16_MIN, 0, -15);
    TEST_CHECK(result == MFX_UINT32_MIN);
}

void test_ConvertP2_s16_s32(void) {
    sint32 result;

    result = Mfx_ConvertP2_s16_s32(32, 4, 5);
    TEST_CHECK(result == 64L);

    result = Mfx_ConvertP2_s16_s32(32, 5, 4);
    TEST_CHECK(result == 16L);

    result = Mfx_ConvertP2_s16_s32(32, 4, 60);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_ConvertP2_s16_s32(32, 60, 4);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_ConvertP2_s16_s32(MFX_SINT16_MAX, 0, 31);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_ConvertP2_s16_s32(MFX_SINT16_MIN, 0, 31);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_MulP2_u16(void) {
    uint16 result;

    result = Mfx_MulP2_u16u16_u16(1, 2, 1, 2, 3);
    TEST_CHECK(result == 2);

    result = Mfx_MulP2_u16u16_u16(1, 2, 0, 0, 10);
    TEST_CHECK(result == 2048);

    result = Mfx_MulP2_u16u16_u16(1, 2, 1, 2, 50);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_MulP2_u16u16_u16(1, 2, 50, 2, 1);
    TEST_CHECK(result == MFX_UINT16_MIN);

    result = Mfx_MulP2_u16u16_u16(MFX_UINT16_MAX, MFX_UINT16_MAX, 0, 0, 15);
    TEST_CHECK(result == MFX_UINT16_MAX);
}

void test_MulP2_s16(void) {
    sint16 result;

    result = Mfx_MulP2_s16s16_s16(1, 2, 1, 2, 3);
    TEST_CHECK(result == 2);

    result = Mfx_MulP2_s16s16_s16(1, 2, 0, 0, 10);
    TEST_CHECK(result == 2048);

    result = Mfx_MulP2_s16s16_s16(1, 2, 1, 2, 50);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_MulP2_s16s16_s16(1, 2, 50, 2, 1);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_MulP2_s16s16_s16(MFX_SINT16_MAX, MFX_SINT16_MAX, 0, 0, 15);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_MulP2_s16s16_s16(MFX_SINT16_MAX, -1, 1, 2, 10);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_MulP2_u32(void) {
    uint32 result;

    result = Mfx_MulP2_u32u32_u32(1UL, 2UL, 1, 2, 3);
    TEST_CHECK(result == 2UL);

    result = Mfx_MulP2_u32u32_u32(1UL, 2UL, 0, 0, 10);
    TEST_CHECK(result == 2048UL);

    result = Mfx_MulP2_u32u32_u32(1UL, 2UL, 1, 2, 500);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_MulP2_u32u32_u32(1UL, 2UL, 500, 2, 1);
    TEST_CHECK(result == MFX_UINT32_MIN);

    result = Mfx_MulP2_u32u32_u32(1000UL, 2000UL, 0, 0, 31);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_MulP2_u32u32_u32(MFX_UINT32_MAX, MFX_UINT32_MAX, 1, 2, 10);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_MulP2_u32u32_u32(MFX_UINT32_MAX, MFX_UINT32_MAX, 0, 0, 31);
    TEST_CHECK(result == MFX_UINT32_MAX);
}

void test_MulP2_s32(void) {
    sint32 result;

    result = Mfx_MulP2_s32s32_s32(1L, 2L, 1, 2, 3);
    TEST_CHECK(result == 2L);

    result = Mfx_MulP2_s32s32_s32(1L, 2L, 0, 0, 10);
    TEST_CHECK(result == 2048L);

    result = Mfx_MulP2_s32s32_s32(1L, 2L, 1, 2, 500);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulP2_s32s32_s32(1L, 2L, 500, 2, 1);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_MulP2_s32s32_s32(MFX_SINT32_MAX, 1L, 0, 0, 31);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulP2_s32s32_s32(MFX_SINT32_MAX, MFX_SINT32_MAX, 0, 0, 31);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_MulP2_s32s32_s32(MFX_SINT32_MAX, MFX_SINT32_MIN, 10, 2, 1);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_DivP2_u16(void) {
    uint16 result;

    result = Mfx_DivP2_u16u16_u16(6, 3, 0, 0, 1);
    TEST_CHECK(result == 4);

    result = Mfx_DivP2_u16u16_u16(6, 3, 5, 0, 1);
    TEST_CHECK(result == 0);

    result = Mfx_DivP2_u16u16_u16(6, 0, 0, 0, 1);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_DivP2_u16u16_u16(6, 3, 0, 0, 50);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_DivP2_u16u16_u16(6, 3, 0, 0, -50);
    TEST_CHECK(result == MFX_UINT16_MIN);

    result = Mfx_DivP2_u16u16_u16(MFX_UINT16_MAX, 1, 0, 0, 30);
    TEST_CHECK(result == MFX_UINT16_MAX);
}

void test_DivP2_s16(void) {
    sint16 result;

    result = Mfx_DivP2_s16s16_s16(6, 3, 0, 0, 1);
    TEST_CHECK(result == 4);

    result = Mfx_DivP2_s16s16_s16(6, 3, 5, 0, 1);
    TEST_CHECK(result == 0);

    result = Mfx_DivP2_s16s16_s16(6, 0, 0, 0, 1);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_DivP2_s16s16_s16(-6, 0, 0, 0, 1);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_DivP2_s16s16_s16(6, 3, 0, 0, 50);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_DivP2_s16s16_s16(6, 3, 0, 0, -50);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_DivP2_s16s16_s16(MFX_SINT16_MAX, 1, 0, 0, 30);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_DivP2_s16s16_s16(MFX_SINT16_MIN, 1, 0, 0, 20);
    TEST_CHECK(result == MFX_SINT16_MIN);
}

void test_DivP2_u32(void) {
    uint32 result;

    result = Mfx_DivP2_u32u32_u32(6UL, 3UL, 0, 0, 1);
    TEST_CHECK(result == 4UL);

    result = Mfx_DivP2_u32u32_u32(6UL, 3UL, 5, 0, 1);
    TEST_CHECK(result == 0UL);

    result = Mfx_DivP2_u32u32_u32(6UL, 0UL, 0, 0, 1);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_DivP2_u32u32_u32(6UL, 3UL, 0, 0, 50);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_DivP2_u32u32_u32(6UL, 3UL, 0, 0, 70);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_DivP2_u32u32_u32(6UL, 3UL, 0, 0, -50);
    TEST_CHECK(result == MFX_UINT32_MIN);

    result = Mfx_DivP2_u32u32_u32(MFX_UINT32_MAX, 1UL, 0, 0, 30);
    TEST_CHECK(result == MFX_UINT32_MAX);
}

void test_DivP2_s32(void) {
    sint32 result;

    result = Mfx_DivP2_s32s32_s32(6L, 3L, 0, 0, 1);
    TEST_CHECK(result == 4L);

    result = Mfx_DivP2_s32s32_s32(6L, 3L, 5, 0, 1);
    TEST_CHECK(result == 0L);

    result = Mfx_DivP2_s32s32_s32(6L, 0L, 0, 0, 1);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivP2_s32s32_s32(-6L, 0L, 0, 0, 1);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_DivP2_s32s32_s32(6L, 3L, 0, 0, 50);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivP2_s32s32_s32(6L, 3L, 0, 0, 70);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivP2_s32s32_s32(6L, 3L, 0, 0, -50);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_DivP2_s32s32_s32(MFX_SINT32_MAX, 1L, 0, 0, 30);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_DivP2_s32s32_s32(MFX_SINT32_MIN, 1L, 0, 0, 20);
    TEST_CHECK(result == MFX_SINT32_MIN);
}

void test_AddP2_u16(void) {
    uint16 result;

    // Invalid range
    result = Mfx_AddP2_u16u16_u16(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_AddP2_u16u16_u16(0, 0, 20, 10, 40);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_AddP2_u16u16_u16(0, 0, 50, 40, 30);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_AddP2_u16u16_u16(0, 0, 30, 40, 60);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_AddP2_u16u16_u16(0, 0, 40, 50, 30);
    TEST_CHECK(result == MFX_UINT16_MAX);

    // Valid range
    result = Mfx_AddP2_u16u16_u16(MFX_UINT16_MAX, MFX_UINT16_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_AddP2_u16u16_u16(10, 10, 1, 3, 4);
    TEST_CHECK(result == 24);

    result = Mfx_AddP2_u16u16_u16(10, 500, 3, 5, 1);
    TEST_CHECK(result == 31);

    result = Mfx_AddP2_u16u16_u16(300, 40, 4, 2, 6);
    TEST_CHECK(result == 1840);
}

void test_AddP2_s16(void) {
    sint16 result;

    // Invalid range
    result = Mfx_AddP2_s16s16_s16(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_AddP2_s16s16_s16(0, 0, 20, 10, 40);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_AddP2_s16s16_s16(0, 0, 50, 40, 30);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_AddP2_s16s16_s16(0, 0, 30, 40, 60);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_AddP2_s16s16_s16(0, 0, 40, 50, 30);
    TEST_CHECK(result == MFX_SINT16_MAX);

    // Valid range
    result = Mfx_AddP2_s16s16_s16(MFX_SINT16_MAX, MFX_SINT16_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_AddP2_s16s16_s16(MFX_SINT16_MIN, MFX_SINT16_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_AddP2_s16s16_s16(300, 40, 4, 2, 6);
    TEST_CHECK(result == 1840);

    result = Mfx_AddP2_s16s16_s16(10, -500, 1, 3, 4);
    TEST_CHECK(result == -996);

    result = Mfx_AddP2_s16s16_s16(10, 500, 3, 5, 1);
    TEST_CHECK(result == 31);
}

void test_AddP2_u32(void) {
    uint32 result;

    // Invalid range
    result = Mfx_AddP2_u32u32_u32(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_AddP2_u32u32_u32(0, 0, 20, 10, 50);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_AddP2_u32u32_u32(0, 0, 50, 40, 10);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_AddP2_u32u32_u32(0, 0, 20, 40, 60);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_AddP2_u32u32_u32(0, 0, 40, 50, 10);
    TEST_CHECK(result == MFX_UINT32_MAX);

    // Valid range
    result = Mfx_AddP2_u32u32_u32(MFX_UINT32_MAX, MFX_UINT32_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_AddP2_u32u32_u32(10, 500, 6, 2, 8);
    TEST_CHECK(result == 32040UL);

    result = Mfx_AddP2_u32u32_u32(10, 500, 1, 3, 4);
    TEST_CHECK(result == 1004UL);

    result = Mfx_AddP2_u32u32_u32(10, 500, 3, 5, 5);
    TEST_CHECK(result == 502UL);
}

void test_AddP2_s32(void) {
    sint32 result;

    // Invalid range
    result = Mfx_AddP2_s32s32_s32(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_AddP2_s32s32_s32(0, 0, 20, 10, 50);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_AddP2_s32s32_s32(0, 0, 50, 40, 10);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_AddP2_s32s32_s32(0, 0, 20, 40, 60);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_AddP2_s32s32_s32(0, 0, 40, 50, 10);
    TEST_CHECK(result == MFX_SINT32_MAX);

    // Valid range
    result = Mfx_AddP2_s32s32_s32(MFX_SINT32_MAX, MFX_SINT32_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_AddP2_s32s32_s32(MFX_SINT32_MIN, MFX_SINT32_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_AddP2_s32s32_s32(100000, 20, 6, 2, 8);
    TEST_CHECK(result == 401280L);

    result = Mfx_AddP2_s32s32_s32(10, -500, 1, 3, 4);
    TEST_CHECK(result == -996L);

    result = Mfx_AddP2_s32s32_s32(-10, -20, 3, 5, 5);
    TEST_CHECK(result == -23L);
}

void test_SubP2_u16(void) {
    uint16 result;

    // Invalid range
    result = Mfx_SubP2_u16u16_u16(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_SubP2_u16u16_u16(0, 0, 20, 10, 40);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_SubP2_u16u16_u16(0, 0, 50, 40, 30);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_SubP2_u16u16_u16(0, 0, 30, 40, 60);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_SubP2_u16u16_u16(0, 0, 40, 50, 30);
    TEST_CHECK(result == MFX_UINT16_MAX);

    // Valid range
    result = Mfx_SubP2_u16u16_u16(MFX_UINT16_MAX, MFX_UINT16_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_UINT16_MAX);

    result = Mfx_SubP2_u16u16_u16(10000, 54, 6, 2, 8);
    TEST_CHECK(result == 36544);

    result = Mfx_SubP2_u16u16_u16(320, 20, 1, 3, 4);
    TEST_CHECK(result == 120);

    result = Mfx_SubP2_u16u16_u16(300, 40, 4, 2, 2);
    TEST_CHECK(result == 35);

    result = Mfx_SubP2_u16u16_u16(10, 500, 3, 5, 5);
    TEST_CHECK(result == MFX_UINT16_MIN);
}

void test_SubP2_s16(void) {
    sint16 result;

    // Invalid range
    result = Mfx_SubP2_s16s16_s16(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_SubP2_s16s16_s16(0, 0, 20, 10, 40);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_SubP2_s16s16_s16(0, 0, 50, 40, 30);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_SubP2_s16s16_s16(0, 0, 30, 40, 60);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_SubP2_s16s16_s16(0, 0, 40, 50, 30);
    TEST_CHECK(result == MFX_SINT16_MAX);

    // Valid range
    result = Mfx_SubP2_s16s16_s16(MFX_SINT16_MAX, MFX_SINT16_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MAX);

    result = Mfx_SubP2_s16s16_s16(MFX_SINT16_MIN, MFX_SINT16_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT16_MIN);

    result = Mfx_SubP2_s16s16_s16(3000, 20, 6, 2, 8);
    TEST_CHECK(result == 10720);

    result = Mfx_SubP2_s16s16_s16(-10, -5, 1, 3, 4);
    TEST_CHECK(result == 4);

    result = Mfx_SubP2_s16s16_s16(10, -50, 3, 5, 5);
    TEST_CHECK(result == 52);
}

void test_SubP2_u32(void) {
    uint32 result;

    // Invalid range
    result = Mfx_SubP2_u32u32_u32(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_SubP2_u32u32_u32(0, 0, 20, 10, 50);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_SubP2_u32u32_u32(0, 0, 50, 40, 10);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_SubP2_u32u32_u32(0, 0, 20, 40, 60);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_SubP2_u32u32_u32(0, 0, 40, 50, 10);
    TEST_CHECK(result == MFX_UINT32_MAX);

    // Valid range
    result = Mfx_SubP2_u32u32_u32(MFX_UINT32_MAX, MFX_UINT32_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_UINT32_MAX);

    result = Mfx_SubP2_u32u32_u32(76100, 50, 6, 2, 8);
    TEST_CHECK(result == 301200UL);

    result = Mfx_SubP2_u32u32_u32(6666, 200, 1, 3, 4);
    TEST_CHECK(result == 2932UL);

    result = Mfx_SubP2_u32u32_u32(200000, 10, 4, 2, 2);
    TEST_CHECK(result == 49990UL);

    result = Mfx_SubP2_u32u32_u32(10, 500, 3, 5, 5);
    TEST_CHECK(result == MFX_UINT32_MIN);
}

void test_SubP2_s32(void) {
    sint32 result;

    // Invalid range
    result = Mfx_SubP2_s32s32_s32(0, 0, 10, 50, 0);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_SubP2_s32s32_s32(0, 0, 20, 10, 50);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_SubP2_s32s32_s32(0, 0, 50, 40, 10);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_SubP2_s32s32_s32(0, 0, 20, 40, 60);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_SubP2_s32s32_s32(0, 0, 40, 50, 10);
    TEST_CHECK(result == MFX_SINT32_MAX);

    // Valid range
    result = Mfx_SubP2_s32s32_s32(MFX_SINT32_MAX, MFX_SINT32_MIN, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT32_MAX);

    result = Mfx_SubP2_s32s32_s32(MFX_SINT32_MIN, MFX_SINT32_MAX, 0, 0, 0);
    TEST_CHECK(result == MFX_SINT32_MIN);

    result = Mfx_SubP2_s32s32_s32(300000, 500, 6, 2, 8);
    TEST_CHECK(result == 1168000L);

    result = Mfx_SubP2_s32s32_s32(-666, 2, 1, 3, 4);
    TEST_CHECK(result == -338L);

    result = Mfx_SubP2_s32s32_s32(-10, -5, 3, 5, 5);
    TEST_CHECK(result == 2L);
}

void test_AbsDiffP2_u16u16_u16(void) {
    uint16 res;

    // |a - b| > 15
    res = Mfx_AbsDiffP2_u16u16_u16(0, 0, 10, 50, 0);
    TEST_CHECK(res == MFX_UINT16_MAX);

    // a >= b: c - b > 15
    res = Mfx_AbsDiffP2_u16u16_u16(0, 0, 20, 10, 40);
    TEST_CHECK(res == MFX_UINT16_MAX);

    // a >= b: a - c > 15
    res = Mfx_AbsDiffP2_u16u16_u16(0, 0, 50, 40, 30);
    TEST_CHECK(res == MFX_UINT16_MAX);

    // a < b: c - a > 15
    res = Mfx_AbsDiffP2_u16u16_u16(0, 0, 30, 40, 60);
    TEST_CHECK(res == MFX_UINT16_MAX);

    // a < b: b - c > 15
    res = Mfx_AbsDiffP2_u16u16_u16(0, 0, 40, 50, 30);
    TEST_CHECK(res == MFX_UINT16_MAX);

    // valid range, a >= b, c - a > 0
    res = Mfx_AbsDiffP2_u16u16_u16(3, 5, 5, 3, 7);
    TEST_CHECK(res == 68);

    // valid range, a >= b, c - a <= 0
    res = Mfx_AbsDiffP2_u16u16_u16(3, 5, 5, 3, 2);
    TEST_CHECK(res == 2);

    // valid range, a < b, c - b > 0
    res = Mfx_AbsDiffP2_u16u16_u16(8, 5, 5, 7, 9);
    TEST_CHECK(res == 108);

    // valid range, a < b, c - b <= 0
    res = Mfx_AbsDiffP2_u16u16_u16(8, 5, 5, 7, 4);
    TEST_CHECK(res == 3);

    // valid range, overflow
    res = Mfx_AbsDiffP2_u16u16_u16(8, 5, 30, 15, 30);
    TEST_CHECK(res == MFX_UINT16_MAX);
}

void test_AbsDiffP2_s16s16_s16(void) {
    uint16 res;

    // |a - b| > 15
    res = Mfx_AbsDiffP2_s16s16_s16(0, 0, 10, 50, 0);
    TEST_CHECK(res == MFX_SINT16_MAX);

    // a >= b: c - b > 15
    res = Mfx_AbsDiffP2_s16s16_s16(0, 0, 20, 10, 40);
    TEST_CHECK(res == MFX_SINT16_MAX);

    // a >= b: a - c > 15
    res = Mfx_AbsDiffP2_s16s16_s16(0, 0, 50, 40, 30);
    TEST_CHECK(res == MFX_SINT16_MAX);

    // a < b: c - a > 15
    res = Mfx_AbsDiffP2_s16s16_s16(0, 0, 30, 40, 60);
    TEST_CHECK(res == MFX_SINT16_MAX);

    // a < b: b - c > 15
    res = Mfx_AbsDiffP2_s16s16_s16(0, 0, 40, 50, 30);
    TEST_CHECK(res == MFX_SINT16_MAX);

    // valid range, a >= b, c - a > 0
    res = Mfx_AbsDiffP2_s16s16_s16(3, 5, 5, 3, 7);
    TEST_CHECK(res == 68);

    // valid range, a >= b, c - a <= 0
    res = Mfx_AbsDiffP2_s16s16_s16(3, 5, 5, 3, 2);
    TEST_CHECK(res == 2);

    // valid range, a < b, c - b > 0
    res = Mfx_AbsDiffP2_s16s16_s16(8, 5, 5, 7, 9);
    TEST_CHECK(res == 108);

    // valid range, a < b, c - b <= 0
    res = Mfx_AbsDiffP2_s16s16_s16(8, 5, 5, 7, 4);
    TEST_CHECK(res == 3);

    // valid range, overflow
    res = Mfx_AbsDiffP2_s16s16_s16(8, 5, 30, 15, 30);
    TEST_CHECK(res == MFX_SINT16_MAX);
}

void test_AbsP2_s16_u16(void) {
    uint16 res;

    res = Mfx_AbsP2_s16_u16(0, 2, 20);
    TEST_CHECK(res == MFX_UINT16_MAX);

    res = Mfx_AbsP2_s16_u16(0, 20, 2);
    TEST_CHECK(res == MFX_UINT16_MIN);

    res = Mfx_AbsP2_s16_u16(-447, 2, 5);
    TEST_CHECK(res == 3576);

    res = Mfx_AbsP2_s16_u16(5555, 5, 2);
    TEST_CHECK(res == 694);

    res = Mfx_AbsP2_s16_u16(1234, 2, 10);
    TEST_CHECK(res == MFX_UINT16_MAX);
}

void test_AbsP2_s16_s16(void) {
    sint16 res;

    res = Mfx_AbsP2_s16_s16(0, 2, 20);
    TEST_CHECK(res == MFX_SINT16_MAX);

    res = Mfx_AbsP2_s16_s16(0, 20, 2);
    TEST_CHECK(res == MFX_SINT16_MIN);

    res = Mfx_AbsP2_s16_s16(-447, 2, 5);
    TEST_CHECK(res == 3576);

    res = Mfx_AbsP2_s16_s16(5555, 5, 2);
    TEST_CHECK(res == 694);

    res = Mfx_AbsP2_s16_s16(1234, 2, 10);
    TEST_CHECK(res == MFX_SINT16_MAX);
}

void test_AbsP2_s32_u32(void) {
    uint32 res;

    res = Mfx_AbsP2_s32_u32(0, 2, 40);
    TEST_CHECK(res == MFX_UINT32_MAX);

    res = Mfx_AbsP2_s32_u32(0, 40, 2);
    TEST_CHECK(res == MFX_UINT32_MIN);

    res = Mfx_AbsP2_s32_u32(-123456L, 4, 10);
    TEST_CHECK(res == 7901184L);

    res = Mfx_AbsP2_s32_u32(50331648L, 12, 2);
    TEST_CHECK(res == 49152L);

    res = Mfx_AbsP2_s32_u32(301989888L, 0, 5);
    TEST_CHECK(res == MFX_UINT32_MAX);
}

void test_AbsP2_s32_s32(void) {
    sint32 res;

    res = Mfx_AbsP2_s32_s32(0, 2, 40);
    TEST_CHECK(res == MFX_SINT32_MAX);

    res = Mfx_AbsP2_s32_s32(0, 40, 2);
    TEST_CHECK(res == MFX_SINT32_MIN);

    res = Mfx_AbsP2_s32_s32(-123456L, 4, 10);
    TEST_CHECK(res == 7901184L);

    res = Mfx_AbsP2_s32_s32(50331648L, 12, 2);
    TEST_CHECK(res == 49152L);

    res = Mfx_AbsP2_s32_s32(301989888L, 0, 5);
    TEST_CHECK(res == MFX_SINT32_MAX);
}

TEST_LIST = {{"GetVersionInfo", test_GetVersionInfo},
             {"Add_8", test_Add_8},
             {"Add_32", test_Add_32},
             {"Sub_8", test_Sub_8},
             {"Sub_32", test_Sub_32},
             {"Abs_8", test_Abs_8},
             {"Abs_32", test_Abs_32},
             {"Mul_8", test_Mul_8},
             {"Mul_32", test_Mul_32},
             {"Div_8", test_Div_8},
             {"Div_32", test_Div_32},
             {"RDiv_8", test_RDiv_8},
             {"RDiv_32", test_RDiv_32},
             {"MulDiv_16", test_MulDiv_16},
             {"MulDiv_32", test_MulDiv_32},
             {"RMulDiv_16", test_RMulDiv_16},
             {"RMulDiv_32", test_RMulDiv_32},
             {"MulShRight_16", test_MulShRight_16},
             {"MulShRight_32", test_MulShRight_32},
             {"DivShLeft_16", test_DivShLeft_16},
             {"DivShLeft_32", test_DivShLeft_32},
             {"Mod_8", test_Mod_8},
             {"Mod_32", test_Mod_32},
             {"Limit_8", test_Limit_8},
             {"Limit_32", test_Limit_32},
             {"Minmax_8", test_Minmax_8},
             {"Minmax_32", test_Minmax_32},
             {"Min_8", test_Min_8},
             {"Min_32", test_Min_32},
             {"Max_8", test_Max_8},
             {"Max_32", test_Max_32},
             {"ConvertP2_u16_u8", test_ConvertP2_u16_u8},
             {"ConvertP2_s16_s8", test_ConvertP2_s16_s8},
             {"ConvertP2_u8_u16", test_ConvertP2_u8_u16},
             {"ConvertP2_s8_s16", test_ConvertP2_s8_s16},
             {"ConvertP2_u32_u16", test_ConvertP2_u32_u16},
             {"ConvertP2_s32_s16", test_ConvertP2_s32_s16},
             {"ConvertP2_u16_u32", test_ConvertP2_u16_u32},
             {"ConvertP2_s16_s32", test_ConvertP2_s16_s32},
             {"MulP2_u16", test_MulP2_u16},
             {"MulP2_s16", test_MulP2_s16},
             {"MulP2_u32", test_MulP2_u32},
             {"MulP2_s32", test_MulP2_s32},
             {"DivP2_u16", test_DivP2_u16},
             {"DivP2_s16", test_DivP2_s16},
             {"DivP2_u32", test_DivP2_u32},
             {"DivP2_s32", test_DivP2_s32},
             {"AddP2_u16", test_AddP2_u16},
             {"AddP2_s16", test_AddP2_s16},
             {"AddP2_u32", test_AddP2_u32},
             {"AddP2_s32", test_AddP2_s32},
             {"SubP2_u16", test_SubP2_u16},
             {"SubP2_s16", test_SubP2_s16},
             {"SubP2_u32", test_SubP2_u32},
             {"SubP2_s32", test_SubP2_s32},
             {"AbsDiffP2_u16u16_u16", test_AbsDiffP2_u16u16_u16},
             {"AbsDiffP2_s16s16_s16", test_AbsDiffP2_s16s16_s16},
             {"AbsP2_s16_u16", test_AbsP2_s16_u16},
             {"AbsP2_s16_s16", test_AbsP2_s16_s16},
             {"AbsP2_s32_u32", test_AbsP2_s32_u32},
             {"AbsP2_s32_s32", test_AbsP2_s32_s32},
             {NULL, NULL}};
