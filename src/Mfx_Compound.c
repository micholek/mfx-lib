#include "Mfx.h"

sint8 Mfx_AbsDiff_s8s8_s8(sint8 x_value, sint8 y_value) {
    return Mfx_Abs_s8_s8(Mfx_Sub_s8s8_s8(x_value, y_value));
}

sint32 Mfx_AbsDiff_s32s32_s32(sint32 x_value, sint32 y_value) {
    return Mfx_Abs_s32_s32(Mfx_Sub_s32s32_s32(x_value, y_value));
}
