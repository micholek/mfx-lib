#include "Mfx.h"

sint8 Mfx_Add_s8s8_s8(sint8 x_value, sint8 y_value) {
    sint16 res = (sint16) x_value + (sint16) y_value;
    if (res > MFX_SINT8_MAX) {
        return MFX_SINT8_MAX;
    } else if (res < MFX_SINT8_MIN) {
        return MFX_SINT8_MIN;
    } else {
        return (sint8) res;
    }
}

sint32 Mfx_Add_s32s32_s32(sint32 x_value, sint32 y_value) {
    sint64 res = (sint64) x_value + (sint64) y_value;
    if (res > MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res < MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

sint8 Mfx_Sub_s8s8_s8(sint8 x_value, sint8 y_value) {
    sint16 res = (sint16) x_value - (sint16) y_value;
    if (res > MFX_SINT8_MAX) {
        return MFX_SINT8_MAX;
    } else if (res < MFX_SINT8_MIN) {
        return MFX_SINT8_MIN;
    } else {
        return (sint8) res;
    }
}

sint32 Mfx_Sub_s32s32_s32(sint32 x_value, sint32 y_value) {
    sint64 res = (sint64) x_value - (sint64) y_value;
    if (res > MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res < MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

sint8 Mfx_Abs_s8_s8(sint8 x_value) {
    if (x_value == MFX_SINT8_MIN) {
        return MFX_SINT8_MAX;
    } else {
        return (x_value < 0) ? -x_value : x_value;
    }
}

sint32 Mfx_Abs_s32_s32(sint32 x_value) {
    if (x_value == MFX_SINT32_MIN) {
        return MFX_SINT32_MAX;
    } else {
        return (x_value < 0) ? -x_value : x_value;
    }
}

sint8 Mfx_Mul_s8s8_s8(sint8 x_value, sint8 y_value) {
    sint16 res = (sint16) x_value * (sint16) y_value;
    if (res > MFX_SINT8_MAX) {
        return MFX_SINT8_MAX;
    } else if (res < MFX_SINT8_MIN) {
        return MFX_SINT8_MIN;
    } else {
        return (sint8) res;
    }
}

sint32 Mfx_Mul_s32s32_s32(sint32 x_value, sint32 y_value) {
    sint64 res = (sint64) x_value * (sint64) y_value;
    if (res > MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res < MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

sint8 Mfx_Div_s8s8_s8(sint8 x_value, sint8 y_value) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT8_MAX : MFX_SINT8_MIN;
    } else {
        sint16 res = (sint16) x_value / (sint16) y_value;
        return (res > MFX_SINT8_MAX) ? MFX_SINT8_MAX : (sint8) res;
    }
}

sint32 Mfx_Div_s32s32_s32(sint32 x_value, sint32 y_value) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    } else {
        sint64 res = (sint64) x_value / (sint64) y_value;
        return (res > MFX_SINT32_MAX) ? MFX_SINT32_MAX : (sint32) res;
    }
}

sint8 Mfx_RDiv_s8s8_s8(sint8 x_value, sint8 y_value) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT8_MAX : MFX_SINT8_MIN;
    } else {
        sint16 result_is_negative = ((x_value ^ y_value) & 0x80) >> 7;
        sint16 sign = result_is_negative * -2 + 1;
        sint16 res = ((sint16) x_value + ((sint16) y_value / 2 * sign)) /
                     (sint16) y_value;
        return (res > MFX_SINT8_MAX) ? MFX_SINT8_MAX : (sint8) res;
    }
}

sint32 Mfx_RDiv_s32s32_s32(sint32 x_value, sint32 y_value) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    } else {
        sint64 result_is_negative = ((x_value ^ y_value) & 0x80000000L) >> 31;
        sint64 sign = result_is_negative * -2L + 1L;
        sint64 res = ((sint64) x_value + ((sint64) y_value / 2L * sign)) /
                     (sint64) y_value;
        return (res > MFX_SINT32_MAX) ? MFX_SINT32_MAX : (sint32) res;
    }
}

sint16 Mfx_MulDiv_s16s16u16_s16(sint16 x_value, sint16 y_value,
                                uint16 z_value) {
    sint32 res = (sint32) x_value * (sint32) y_value;

    if (z_value == 0) {
        return (res >= 0) ? MFX_SINT16_MAX : MFX_SINT16_MIN;
    } else {
        res /= z_value;
        if (res >= MFX_SINT16_MAX) {
            return MFX_SINT16_MAX;
        } else if (res <= MFX_SINT16_MIN) {
            return MFX_SINT16_MIN;
        } else {
            return (sint16) res;
        }
    }
}

sint32 Mfx_MulDiv_s32s32s32_s32(sint32 x_value, sint32 y_value,
                                sint32 z_value) {
    sint64 res = (sint64) x_value * (sint64) y_value;

    if (z_value == 0) {
        return (res >= 0) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    } else {
        res /= z_value;
        if (res >= MFX_SINT32_MAX) {
            return MFX_SINT32_MAX;
        } else if (res <= MFX_SINT32_MIN) {
            return MFX_SINT32_MIN;
        } else {
            return (sint32) res;
        }
    }
}

sint16 Mfx_RMulDiv_s16s16s16_s16(sint16 x_value, sint16 y_value,
                                 sint16 z_value) {
    sint32 res = (sint32) x_value * (sint32) y_value;

    if (z_value == 0) {
        return (res >= 0) ? MFX_SINT16_MAX : MFX_SINT16_MIN;
    } else {
        sint32 is_negative = ((res ^ (sint32) z_value) >> 31) & 0x1;
        sint32 sign = is_negative * -2L + 1L;

        res = (res + ((sint32) z_value / 2L * sign)) / z_value;

        if (res >= MFX_SINT16_MAX) {
            return MFX_SINT16_MAX;
        } else if (res <= MFX_SINT16_MIN) {
            return MFX_SINT16_MIN;
        } else {
            return (sint16) res;
        }
    }
}

sint32 Mfx_RMulDiv_s32s32s32_s32(sint32 x_value, sint32 y_value,
                                 sint32 z_value) {
    sint64 res = (sint64) x_value * (sint64) y_value;

    if (z_value == 0) {
        return (res >= 0) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    } else {
        sint64 is_negative = ((res ^ (sint64) z_value) >> 63) & 0x1;
        sint64 sign = is_negative * -2L + 1L;

        res = (res + ((sint64) z_value / 2L * sign)) / z_value;

        if (res >= MFX_SINT32_MAX) {
            return MFX_SINT32_MAX;
        } else if (res <= MFX_SINT32_MIN) {
            return MFX_SINT32_MIN;
        } else {
            return (sint32) res;
        }
    }
}

sint16 Mfx_MulShRight_s16s16u8_s16(sint16 x_value, sint16 y_value,
                                   uint8 shift) {
    static const uint8 max_shift = 30;
    sint32 res = ((sint32) x_value * (sint32) y_value);
    res >>= (shift > max_shift) ? max_shift : shift;

    if (res >= MFX_SINT16_MAX) {
        return MFX_SINT16_MAX;
    } else if (res <= MFX_SINT16_MIN) {
        return MFX_SINT16_MIN;
    } else {
        return (sint16) res;
    }
}

sint32 Mfx_MulShRight_s32s32u8_s32(sint32 x_value, sint32 y_value,
                                   uint8 shift) {
    static const uint8 max_shift = 62;
    sint64 res = ((sint64) x_value * (sint64) y_value);
    res >>= (shift > max_shift) ? max_shift : shift;

    if (res >= MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res <= MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

sint16 Mfx_DivShLeft_s16s16u8_s16(sint16 x_value, sint16 y_value, uint8 shift) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT16_MAX : MFX_SINT16_MIN;
    }

    static const uint8 max_shift = 15;
    sint32 res = (sint32) x_value << ((shift >= max_shift) ? max_shift : shift);
    res /= y_value;

    if (res >= MFX_SINT16_MAX) {
        return MFX_SINT16_MAX;
    } else if (res <= MFX_SINT16_MIN) {
        return MFX_SINT16_MIN;
    } else {
        return (sint16) res;
    }
}

sint32 Mfx_DivShLeft_s32s32u8_s32(sint32 x_value, sint32 y_value, uint8 shift) {
    if (y_value == 0) {
        return (x_value >= 0) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    }

    static const uint8 max_shift = 31;
    sint64 res = (sint64) x_value << ((shift >= max_shift) ? max_shift : shift);
    res /= y_value;

    if (res >= MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res <= MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

sint8 Mfx_Mod_s8(sint8 x_value, sint8 y_value) {
    if ((y_value == 0) || ((x_value == MFX_SINT8_MIN) && (y_value == -1))) {
        return 0;
    } else {
        return x_value % y_value;
    }
}

sint32 Mfx_Mod_s32(sint32 x_value, sint32 y_value) {
    if ((y_value == 0) || ((x_value == MFX_SINT32_MIN) && (y_value == -1L))) {
        return 0;
    } else {
        return x_value % y_value;
    }
}

sint8 Mfx_Limit_s8(sint8 value, sint8 min_value, sint8 max_value) {
    if (value < min_value) {
        return min_value;
    } else if (value > max_value) {
        return max_value;
    } else {
        return value;
    }
}

sint32 Mfx_Limit_s32(sint32 value, sint32 min_value, sint32 max_value) {
    if (value < min_value) {
        return min_value;
    } else if (value > max_value) {
        return max_value;
    } else {
        return value;
    }
}

sint8 Mfx_Minmax_s8(sint8 value, sint8 minmax_value) {
    if ((minmax_value >= 0) && (value > minmax_value)) {
        return minmax_value;
    } else if ((minmax_value < 0) && (value < minmax_value)) {
        return minmax_value;
    } else {
        return value;
    }
}

sint32 Mfx_Minmax_s32(sint32 value, sint32 minmax_value) {
    if ((minmax_value >= 0) && (value > minmax_value)) {
        return minmax_value;
    } else if ((minmax_value < 0) && (value < minmax_value)) {
        return minmax_value;
    } else {
        return value;
    }
}

sint8 Mfx_Min_s8(sint8 x_value, sint8 y_value) {
    return (x_value < y_value) ? x_value : y_value;
}

sint32 Mfx_Min_s32(sint32 x_value, sint32 y_value) {
    return (x_value < y_value) ? x_value : y_value;
}

sint8 Mfx_Max_s8(sint8 x_value, sint8 y_value) {
    return (x_value > y_value) ? x_value : y_value;
}

sint32 Mfx_Max_s32(sint32 x_value, sint32 y_value) {
    return (x_value > y_value) ? x_value : y_value;
}

uint8 Mfx_ConvertP2_u16_u8(uint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 7) {
        return MFX_UINT8_MAX;
    } else if (diff_c_a < -15) {
        return MFX_UINT8_MIN;
    } else {
        uint32 res = (diff_c_a > 0) ? ((uint32) x << diff_c_a)
                                    : ((uint32) x >> -diff_c_a);
        return (res > MFX_UINT8_MAX) ? MFX_UINT8_MAX : (uint8) res;
    }
}

sint8 Mfx_ConvertP2_s16_s8(sint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 7) {
        return MFX_SINT8_MAX;
    } else if (diff_c_a < -15) {
        return MFX_SINT8_MIN;
    } else {
        sint32 res = (diff_c_a > 0) ? ((sint32) x << diff_c_a)
                                    : ((sint32) x >> -diff_c_a);
        if (res >= MFX_SINT8_MAX) {
            return MFX_SINT8_MAX;
        } else if (res <= MFX_SINT8_MIN) {
            return MFX_SINT8_MIN;
        } else {
            return (sint8) res;
        }
    }
}

uint16 Mfx_ConvertP2_u8_u16(uint8 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_UINT16_MAX;
    } else if (diff_c_a < -7) {
        return MFX_UINT16_MIN;
    } else {
        uint32 res = (diff_c_a > 0) ? ((uint32) x << diff_c_a)
                                    : ((uint32) x >> -diff_c_a);
        return (res > MFX_UINT16_MAX) ? MFX_UINT16_MAX : (uint16) res;
    }
}

sint16 Mfx_ConvertP2_s8_s16(sint8 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_SINT16_MAX;
    } else if (diff_c_a < -7) {
        return MFX_SINT16_MIN;
    } else {
        sint32 res = (diff_c_a > 0) ? ((sint32) x << diff_c_a)
                                    : ((sint32) x >> -diff_c_a);
        if (res >= MFX_SINT16_MAX) {
            return MFX_SINT16_MAX;
        } else if (res <= MFX_SINT16_MIN) {
            return MFX_SINT16_MIN;
        } else {
            return (sint16) res;
        }
    }
}

uint16 Mfx_ConvertP2_u32_u16(uint32 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_UINT16_MAX;
    } else if (diff_c_a < -31) {
        return MFX_UINT16_MIN;
    } else {
        uint64 res = (diff_c_a > 0) ? ((uint64) x << diff_c_a)
                                    : ((uint64) x >> -diff_c_a);
        return (res > MFX_UINT16_MAX) ? MFX_UINT16_MAX : (uint16) res;
    }
}

sint16 Mfx_ConvertP2_s32_s16(sint32 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_SINT16_MAX;
    } else if (diff_c_a < -31) {
        return MFX_SINT16_MIN;
    } else {
        sint64 res = (diff_c_a > 0) ? ((sint64) x << diff_c_a)
                                    : ((sint64) x >> -diff_c_a);
        if (res >= MFX_SINT16_MAX) {
            return MFX_SINT16_MAX;
        } else if (res <= MFX_SINT16_MIN) {
            return MFX_SINT16_MIN;
        } else {
            return (sint16) res;
        }
    }
}

uint32 Mfx_ConvertP2_u16_u32(uint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 31) {
        return MFX_UINT32_MAX;
    } else if (diff_c_a < -15) {
        return MFX_UINT32_MIN;
    } else {
        uint64 res = (diff_c_a > 0) ? ((uint64) x << diff_c_a)
                                    : ((uint64) x >> -diff_c_a);
        return (res > MFX_UINT32_MAX) ? MFX_UINT32_MAX : (uint32) res;
    }
}

sint32 Mfx_ConvertP2_s16_s32(sint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 31) {
        return MFX_SINT32_MAX;
    } else if (diff_c_a < -15) {
        return MFX_SINT32_MIN;
    } else {
        sint64 res = (diff_c_a > 0) ? ((sint64) x << diff_c_a)
                                    : ((sint64) x >> -diff_c_a);
        if (res >= MFX_SINT32_MAX) {
            return MFX_SINT32_MAX;
        } else if (res <= MFX_SINT32_MIN) {
            return MFX_SINT32_MIN;
        } else {
            return (sint32) res;
        }
    }
}

uint16 Mfx_MulP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_c_b_a = (sint32) c - (sint32) b - (sint32) a;
    if (diff_c_b_a > 15) {
        return MFX_UINT16_MAX;
    } else if (diff_c_b_a < -31) {
        return MFX_UINT16_MIN;
    } else {
        uint64 res = (diff_c_b_a > 0)
                         ? ((((uint64) x) * ((uint64) y)) << diff_c_b_a)
                         : ((((uint64) x) * ((uint64) y)) >> -diff_c_b_a);
        return (res > MFX_UINT16_MAX) ? MFX_UINT16_MAX : (uint16) res;
    }
}

sint16 Mfx_MulP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_c_b_a = (sint32) c - (sint32) b - (sint32) a;
    if (diff_c_b_a > 15) {
        return MFX_SINT16_MAX;
    } else if (diff_c_b_a < -31) {
        return MFX_SINT16_MIN;
    } else {
        sint64 res = (diff_c_b_a > 0)
                         ? ((((sint64) x) * ((sint64) y)) << diff_c_b_a)
                         : ((((sint64) x) * ((sint64) y)) >> -diff_c_b_a);
        if (res >= MFX_SINT16_MAX) {
            return MFX_SINT16_MAX;
        } else if (res <= MFX_SINT16_MIN) {
            return MFX_SINT16_MIN;
        } else {
            return (sint16) res;
        }
    }
}

uint32 Mfx_MulP2_u32u32_u32(uint32 x, uint32 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_c_b_a = (sint32) c - (sint32) b - (sint32) a;
    if (diff_c_b_a > 31) {
        return MFX_UINT32_MAX;
    } else if (diff_c_b_a < -63) {
        return MFX_UINT32_MIN;
    } else {
        uint64 res;
        uint64 mul_res = ((uint64) x) * ((uint64) y);

        if (diff_c_b_a > 0) {
            res = (mul_res >= MFX_UINT32_MAX) ? MFX_UINT32_MAX
                                              : (mul_res << diff_c_b_a);
        } else {
            res = (mul_res >> -diff_c_b_a);
        }

        return (res > MFX_UINT32_MAX) ? MFX_UINT32_MAX : (uint32) res;
    }
}

sint32 Mfx_MulP2_s32s32_s32(sint32 x, sint32 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_c_b_a = (sint32) c - (sint32) b - (sint32) a;
    if (diff_c_b_a > 31) {
        return MFX_SINT32_MAX;
    } else if (diff_c_b_a < -63) {
        return MFX_SINT32_MIN;
    } else {
        sint64 res;
        sint64 mul_res = ((sint64) x) * ((sint64) y);

        if (diff_c_b_a > 0) {
            res = (mul_res >= MFX_SINT32_MAX) ? MFX_SINT32_MAX
                                              : (mul_res << diff_c_b_a);
        } else {
            res = (mul_res <= MFX_SINT32_MIN) ? MFX_SINT32_MIN
                                              : (mul_res >> -diff_c_b_a);
        }

        if (res >= MFX_SINT32_MAX) {
            return MFX_SINT32_MAX;
        } else if (res <= MFX_SINT32_MIN) {
            return MFX_SINT32_MIN;
        } else {
            return (sint32) res;
        }
    }
}

uint16 Mfx_DivP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 c_add_b_minus_a = (sint32) c + (sint32) b - (sint32) a;
    if (y == 0) {
        return MFX_UINT16_MAX;
    } else {
        if (c_add_b_minus_a > 31) {
            return MFX_UINT16_MAX;
        } else if (c_add_b_minus_a < -15) {
            return MFX_UINT16_MIN;
        } else {
            uint64 res = (c_add_b_minus_a > 0)
                             ? (((uint64) x) << c_add_b_minus_a) / (uint64) y
                             : (((uint64) x) >> -c_add_b_minus_a) / (uint64) y;
            return (res > MFX_UINT16_MAX) ? MFX_UINT16_MAX : (uint16) res;
        }
    }
}

sint16 Mfx_DivP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 c_add_b_minus_a = (sint32) c + (sint32) b - (sint32) a;
    if (y == 0) {
        return (x >= 0) ? MFX_SINT16_MAX : MFX_SINT16_MIN;
    } else {
        if (c_add_b_minus_a > 31) {
            return MFX_SINT16_MAX;
        } else if (c_add_b_minus_a < -15) {
            return MFX_SINT16_MIN;
        } else {
            sint64 res = (c_add_b_minus_a > 0)
                             ? (((sint64) x) << c_add_b_minus_a) / (sint64) y
                             : (((sint64) x) >> -c_add_b_minus_a) / (sint64) y;

            if (res >= MFX_SINT16_MAX) {
                return MFX_SINT16_MAX;
            } else if (res <= MFX_SINT16_MIN) {
                return MFX_SINT16_MIN;
            } else {
                return (sint16) res;
            }
        }
    }
}

uint32 Mfx_DivP2_u32u32_u32(uint32 x, uint32 y, sint16 a, sint16 b, sint16 c) {
    const sint32 c_add_b_minus_a = (sint32) c + (sint32) b - (sint32) a;
    if (y == 0UL) {
        return MFX_UINT32_MAX;
    } else {
        if (c_add_b_minus_a > 63) {
            return MFX_UINT32_MAX;
        } else if (c_add_b_minus_a < -31) {
            return MFX_UINT32_MIN;
        } else {
            uint64 res = (c_add_b_minus_a > 0)
                             ? (((uint64) x) << c_add_b_minus_a) / (uint64) y
                             : (((uint64) x) >> -c_add_b_minus_a) / (uint64) y;
            return (res > MFX_UINT32_MAX) ? MFX_UINT32_MAX : (uint32) res;
        }
    }
}

sint32 Mfx_DivP2_s32s32_s32(sint32 x, sint32 y, sint16 a, sint16 b, sint16 c) {
    const sint32 c_add_b_minus_a = (sint32) c + (sint32) b - (sint32) a;
    if (y == 0L) {
        return (x >= 0L) ? MFX_SINT32_MAX : MFX_SINT32_MIN;
    } else {
        if (c_add_b_minus_a > 63) {
            return MFX_SINT32_MAX;
        } else if (c_add_b_minus_a < -31) {
            return MFX_SINT32_MIN;
        } else {
            sint64 res = (c_add_b_minus_a > 0)
                             ? (((sint64) x) << c_add_b_minus_a) / (sint64) y
                             : (((sint64) x) >> -c_add_b_minus_a) / (sint64) y;

            if (res >= MFX_SINT32_MAX) {
                return MFX_SINT32_MAX;
            } else if (res <= MFX_SINT32_MIN) {
                return MFX_SINT32_MIN;
            } else {
                return (sint32) res;
            }
        }
    }
}

uint16 Mfx_AddP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 abs_diff_a_b = (diff_a_b > 0) ? diff_a_b : (-diff_a_b);
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_UINT16_MAX;
    }

    uint32 res;
    if (a >= b) {
        uint32 sum = (uint32) x + ((uint32) y << diff_a_b);
        res = (diff_c_a > 0) ? (sum << diff_c_a) : (sum >> (-diff_c_a));
    } else {
        uint32 sum = ((uint32) x >> (-diff_a_b)) + (uint32) y;
        res = (diff_c_b > 0) ? (sum << diff_c_b) : (sum >> (-diff_c_b));
    }
    return (res > MFX_UINT16_MAX) ? MFX_UINT16_MAX : (uint16) res;
}

sint16 Mfx_AddP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 abs_diff_a_b = (diff_a_b > 0) ? diff_a_b : (-diff_a_b);
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_SINT16_MAX;
    }

    sint32 res;
    if (a >= b) {
        sint32 sum = (sint32) x + ((sint32) y << diff_a_b);
        res = (diff_c_a > 0) ? (sum << diff_c_a) : (sum >> (-diff_c_a));
    } else {
        sint32 sum = ((sint32) x >> abs_diff_a_b) + (sint32) y;
        res = (diff_c_b > 0) ? (sum << diff_c_b) : (sum >> (-diff_c_b));
    }

    if (res >= MFX_SINT16_MAX) {
        return MFX_SINT16_MAX;
    } else if (res <= MFX_SINT16_MIN) {
        return MFX_SINT16_MIN;
    } else {
        return (sint16) res;
    }
}

uint32 Mfx_AddP2_u32u32_u32(uint32 x, uint32 y, sint32 a, sint32 b, sint32 c) {
    const sint64 diff_a_b = (sint64) a - (sint64) b;
    const sint64 diff_c_a = (sint64) c - (sint64) a;
    const sint64 diff_c_b = (sint64) c - (sint64) b;
    const sint64 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint64 diff_a_c = (sint64) a - (sint64) c;
    const sint64 diff_b_c = (sint64) b - (sint64) c;

    if (abs_diff_a_b > 31 || (a >= b && (diff_c_b > 31 || diff_a_c > 31)) ||
        (a < b && (diff_c_a > 31 || diff_b_c > 31))) {
        return MFX_UINT32_MAX;
    }

    uint64 res;
    if (a >= b) {
        uint64 sum = (uint64) x + ((uint64) y << diff_a_b);
        res = (diff_c_a > 0) ? (sum << diff_c_a) : (sum >> (-diff_c_a));
    } else {
        uint64 sum = ((uint64) x >> abs_diff_a_b) + (uint64) y;
        res = (diff_c_b > 0) ? (sum << diff_c_b) : (sum >> (-diff_c_b));
    }
    return (res > MFX_UINT32_MAX) ? MFX_UINT32_MAX : (uint32) res;
}

sint32 Mfx_AddP2_s32s32_s32(sint32 x, sint32 y, sint32 a, sint32 b, sint32 c) {
    const sint64 diff_a_b = (sint64) a - (sint64) b;
    const sint64 diff_c_a = (sint64) c - (sint64) a;
    const sint64 diff_c_b = (sint64) c - (sint64) b;
    const sint64 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint64 diff_a_c = (sint64) a - (sint64) c;
    const sint64 diff_b_c = (sint64) b - (sint64) c;

    if (abs_diff_a_b > 31 || (a >= b && (diff_c_b > 31 || diff_a_c > 31)) ||
        (a < b && (diff_c_a > 31 || diff_b_c > 31))) {
        return MFX_SINT32_MAX;
    }

    sint64 res;
    if (a >= b) {
        sint64 sum = (sint64) x + ((sint64) y << diff_a_b);
        res = (diff_c_a > 0) ? (sum << diff_c_a) : (sum >> (-diff_c_a));
    } else {
        sint64 sum = ((sint64) x >> abs_diff_a_b) + (sint64) y;
        res = (diff_c_b > 0) ? (sum << diff_c_b) : (sum >> (-diff_c_b));
    }

    if (res >= MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res <= MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

uint16 Mfx_SubP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_UINT16_MAX;
    }

    sint32 res;
    if (a >= b) {
        sint32 diff = (sint32) x - ((sint32) y << diff_a_b);
        res = (diff_c_a > 0) ? (diff << diff_c_a) : (diff >> (-diff_c_a));
    } else {
        sint32 diff = ((sint32) x >> abs_diff_a_b) - (sint32) y;
        res = (diff_c_b > 0) ? (diff << diff_c_b) : (diff >> (-diff_c_b));
    }

    if (res >= MFX_UINT16_MAX) {
        return MFX_UINT16_MAX;
    } else if (res <= MFX_UINT16_MIN) {
        return MFX_UINT16_MIN;
    } else {
        return (uint16) res;
    }
}

sint16 Mfx_SubP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b, sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_SINT16_MAX;
    }

    sint32 res;
    if (a >= b) {
        sint32 diff = (sint32) x - ((sint32) y << diff_a_b);
        res = (diff_c_a > 0) ? (diff << diff_c_a) : (diff >> (-diff_c_a));
    } else {
        sint32 diff = ((sint32) x >> abs_diff_a_b) - (sint32) y;
        res = (diff_c_b > 0) ? (diff << diff_c_b) : (diff >> (-diff_c_b));
    }

    if (res >= MFX_SINT16_MAX) {
        return MFX_SINT16_MAX;
    } else if (res <= MFX_SINT16_MIN) {
        return MFX_SINT16_MIN;
    } else {
        return (sint16) res;
    }
}

uint32 Mfx_SubP2_u32u32_u32(uint32 x, uint32 y, sint32 a, sint32 b, sint32 c) {
    const sint64 diff_a_b = (sint64) a - (sint64) b;
    const sint64 diff_c_a = (sint64) c - (sint64) a;
    const sint64 diff_c_b = (sint64) c - (sint64) b;
    const sint64 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint64 diff_a_c = (sint64) a - (sint64) c;
    const sint64 diff_b_c = (sint64) b - (sint64) c;

    if (abs_diff_a_b > 31 || (a >= b && (diff_c_b > 31 || diff_a_c > 31)) ||
        (a < b && (diff_c_a > 31 || diff_b_c > 31))) {
        return MFX_UINT32_MAX;
    }

    sint64 res;
    if (a >= b) {
        sint64 diff = (sint64) x - ((sint64) y << diff_a_b);
        res = (diff_c_a > 0) ? (diff << diff_c_a) : (diff >> (-diff_c_a));
    } else {
        sint64 diff = ((sint64) x >> abs_diff_a_b) - (sint64) y;
        res = (diff_c_b > 0) ? (diff << diff_c_b) : (diff >> (-diff_c_b));
    }

    if (res >= (sint64) MFX_UINT32_MAX) {
        return MFX_UINT32_MAX;
    } else if (res <= (sint64) MFX_UINT32_MIN) {
        return MFX_UINT32_MIN;
    } else {
        return (uint32) res;
    }
}

sint32 Mfx_SubP2_s32s32_s32(sint32 x, sint32 y, sint32 a, sint32 b, sint32 c) {
    const sint64 diff_a_b = (sint64) a - (sint64) b;
    const sint64 diff_c_a = (sint64) c - (sint64) a;
    const sint64 diff_c_b = (sint64) c - (sint64) b;
    const sint64 abs_diff_a_b = (diff_a_b < 0) ? (-diff_a_b) : diff_a_b;
    const sint64 diff_a_c = (sint64) a - (sint64) c;
    const sint64 diff_b_c = (sint64) b - (sint64) c;

    if (abs_diff_a_b > 31 || (a >= b && (diff_c_b > 31 || diff_a_c > 31)) ||
        (a < b && (diff_c_a > 31 || diff_b_c > 31))) {
        return MFX_SINT32_MAX;
    }

    sint64 res;
    if (a >= b) {
        sint64 diff = (sint64) x - ((sint64) y << diff_a_b);
        res = (diff_c_a > 0) ? (diff << diff_c_a) : (diff >> (-diff_c_a));
    } else {
        sint64 diff = ((sint64) x >> (-diff_a_b)) - (sint64) y;
        res = (diff_c_b > 0) ? (diff << diff_c_b) : (diff >> (-diff_c_b));
    }

    if (res >= MFX_SINT32_MAX) {
        return MFX_SINT32_MAX;
    } else if (res <= MFX_SINT32_MIN) {
        return MFX_SINT32_MIN;
    } else {
        return (sint32) res;
    }
}

uint16 Mfx_AbsDiffP2_u16u16_u16(uint16 x, uint16 y, sint16 a, sint16 b,
                                sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 abs_diff_a_b = diff_a_b < 0 ? -diff_a_b : diff_a_b;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_UINT16_MAX;
    }

    uint32 res;
    if (a >= b) {
        sint32 diff = (sint32) x - ((uint32) y << diff_a_b);
        uint32 abs_diff = diff < 0 ? -diff : diff;
        res = diff_c_a > 0 ? (abs_diff << diff_c_a) : (abs_diff >> diff_a_c);
    } else {
        sint32 diff = (sint32) ((uint32) x << abs_diff_a_b) - y;
        uint32 abs_diff = diff < 0 ? -diff : diff;
        res = diff_c_b > 0 ? (abs_diff << diff_c_b) : (abs_diff >> diff_b_c);
    }

    return res > MFX_UINT16_MAX ? MFX_UINT16_MAX : (uint16) res;
}

sint16 Mfx_AbsDiffP2_s16s16_s16(sint16 x, sint16 y, sint16 a, sint16 b,
                                sint16 c) {
    const sint32 diff_a_b = (sint32) a - (sint32) b;
    const sint32 abs_diff_a_b = diff_a_b < 0 ? -diff_a_b : diff_a_b;
    const sint32 diff_c_b = (sint32) c - (sint32) b;
    const sint32 diff_a_c = (sint32) a - (sint32) c;
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    const sint32 diff_b_c = (sint32) b - (sint32) c;

    if (abs_diff_a_b > 15 || (a >= b && (diff_c_b > 15 || diff_a_c > 15)) ||
        (a < b && (diff_c_a > 15 || diff_b_c > 15))) {
        return MFX_SINT16_MAX;
    }

    sint32 res;
    if (a >= b) {
        sint32 diff = (sint32) x - ((uint32) y << diff_a_b);
        uint32 abs_diff = diff < 0 ? -diff : diff;
        res = diff_c_a > 0 ? (abs_diff << diff_c_a) : (abs_diff >> diff_a_c);
    } else {
        sint32 diff = (sint32) ((uint32) x << abs_diff_a_b) - y;
        uint32 abs_diff = diff < 0 ? -diff : diff;
        res = diff_c_b > 0 ? (abs_diff << diff_c_b) : (abs_diff >> diff_b_c);
    }

    return res > MFX_SINT16_MAX ? MFX_SINT16_MAX : (sint16) res;
}

uint16 Mfx_AbsP2_s16_u16(sint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_UINT16_MAX;
    } else if (diff_c_a < -15) {
        return MFX_UINT16_MIN;
    } else {
        uint32 abs_x = x < 0 ? -x : x;
        uint32 res =
            (diff_c_a > 0) ? (abs_x << diff_c_a) : (abs_x >> -diff_c_a);
        return res > MFX_UINT16_MAX ? MFX_UINT16_MAX : (uint16) res;
    }
}

sint16 Mfx_AbsP2_s16_s16(sint16 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 15) {
        return MFX_SINT16_MAX;
    } else if (diff_c_a < -15) {
        return MFX_SINT16_MIN;
    } else {
        uint32 abs_x = x < 0 ? -x : x;
        uint32 res =
            (diff_c_a > 0) ? (abs_x << diff_c_a) : (abs_x >> -diff_c_a);
        return res > MFX_SINT16_MAX ? MFX_SINT16_MAX : (sint16) res;
    }
}

uint32 Mfx_AbsP2_s32_u32(sint32 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 31) {
        return MFX_UINT32_MAX;
    } else if (diff_c_a < -31) {
        return MFX_UINT32_MIN;
    } else {
        uint64 abs_x = x < 0 ? -x : x;
        uint64 res =
            (diff_c_a > 0) ? (abs_x << diff_c_a) : (abs_x >> -diff_c_a);
        return res > MFX_UINT32_MAX ? MFX_UINT32_MAX : (uint32) res;
    }
}

sint32 Mfx_AbsP2_s32_s32(sint32 x, sint16 a, sint16 c) {
    const sint32 diff_c_a = (sint32) c - (sint32) a;
    if (diff_c_a > 31) {
        return MFX_SINT32_MAX;
    } else if (diff_c_a < -31) {
        return MFX_SINT32_MIN;
    } else {
        uint64 abs_x = x < 0 ? -x : x;
        uint64 res =
            (diff_c_a > 0) ? (abs_x << diff_c_a) : (abs_x >> -diff_c_a);
        return res > MFX_SINT32_MAX ? MFX_SINT32_MAX : (sint32) res;
    }
}
